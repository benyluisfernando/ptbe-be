// Update with your config settings.

module.exports = {
  development: {
    client: "mssql",
    connection: {
     host: "182.169.41.152",
     port: 1433,
     database: "UbpPortal",
     user: "ubp",
     password: "12345Ubp",
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "pgmigra",
    },
  },
  // development: {
  //   client: "oracledb",
  //   connection: {
  //     user: "KKTORA",
  //     password: "P@ssw0rd.1", // mypw contains the hr schema password
  //     connectString: "182.169.41.225:1521/XE",
  //     database: "KKTORA",
  //   },
  //   migrations: {
  //     tableName: "oram",
  //   },
  // },
};
