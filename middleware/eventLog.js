const pool = require("../connection/db");

module.exports = async (req, res, next) => {
  if (req.url != "who") {
    await insertIntoLogEvent(req);
  }
  if (!req.url.includes("signout")) {
    try {
      const oldWrite = res.write;
      const oldEnd = res.end;

      const chunks = [];

      res.write = (...restArgs) => {
        chunks.push(Buffer.from(restArgs[0]));
        oldWrite.apply(res, restArgs);
      };

      res.end = async (...restArgs) => {
        if (restArgs[0]) {
          chunks.push(Buffer.from(restArgs[0]));
        }
        const body = Buffer.concat(chunks).toString("utf8");
        if (req.url.includes("sign")) {
          req.body.secret = "**************";
        }

        if (req.url != "who") {
          // await pool("event_log")
          //   .returning(["*"])
          //   .insert({
          //     request_ip:
          //       req.headers["x-forwarded-for"] || req.connection.remoteAddress,
          //     request_url: req.originalUrl,
          //     request_method: req.method,
          //     request_service: req.url,
          //     request_body: JSON.stringify(req.body),
          //     request_header: JSON.stringify(req.headers),
          //     request_date: req.requestDate,
          //     response_data: JSON.stringify(body) || {},
          //     response_date: new Date(),
          //     request_token: req.tokenDate,
          //     app_id: req.userData ? req.userData.apps[0].idapp : null,
          //   });
        }
        //console.log(res, restArgs);

        oldEnd.apply(res, restArgs);
      };
    } catch (err) {
      console.log(err);
      next();
    }
  }
  next();
};

const insertIntoLogEvent = async (req) => {
  try {
    let fullPath = req.baseUrl + req.path;
    let routes = req.baseUrl.split("/");

    let eventVal =
      fullPath.includes("getAll") || fullPath.includes("retrive")
        ? `${routes[2]} View all data `
        : fullPath.includes("get") || fullPath.includes("byid")
        ? `${routes[2]} View data with filter`
        : fullPath.includes("insert") || fullPath.includes("add")
        ? `${routes[2]} Insert new data `
        : fullPath.includes("update") ||
          fullPath.includes("edit") ||
          fullPath.includes("change")
        ? `${routes[2]} Update data `
        : fullPath.includes("delete")
        ? `${routes[2]} Delete data`
        : "";
    console.log(req.userData);
    let idapp = req.userData.apps[0].idapp;

    let description =
      fullPath.includes("getAll") || fullPath.includes("retrive")
        ? `${routes[2]} View all ${routes[2]} data `
        : fullPath.includes("get") || fullPath.includes("byid")
        ? `${routes[2]} View ${routes[2]} data with filter`
        : fullPath.includes("insert") || fullPath.includes("add")
        ? `${routes[2]} Insert ${routes[2]} new data `
        : fullPath.includes("update") ||
          fullPath.includes("edit") ||
          fullPath.includes("change")
        ? `${routes[2]} Update ${routes[2]} data `
        : fullPath.includes("delete")
        ? `${routes[2]} Delete ${routes[2]} data`
        : "";
    let cdaction =
      fullPath.includes("getAll") || fullPath.includes("retrive")
        ? 4
        : fullPath.includes("get") || fullPath.includes("byid")
        ? 4
        : fullPath.includes("insert") || fullPath.includes("add")
        ? 5
        : fullPath.includes("update") ||
          fullPath.includes("edit") ||
          fullPath.includes("change")
        ? 6
        : fullPath.includes("delete")
        ? 7
        : 4;
    console.log("EVENTTTTTTTTTTTTTT");
    console.log(eventVal, idapp, 0);

    await pool("trx_eventlog").insert({
      created_by: req.userData.id ? req.userData.id : 0,
      value: eventVal ? eventVal : 0,
      idapp: idapp ? idapp : 0,
      idmenu: 0,
      description: description ? description : 0,
      cdaction: cdaction ? cdaction : 0,
      refevent: req.tokenID ? req.tokenID : 0,
      valuejson: req.body ? JSON.stringify(req.body) : null,
    });
    // fileLog.info(`Saving event to eventlog `);
  } catch (err) {
    console.log(err);
    //next();
  }
};
