(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\workspaces\angularws\krakatoa-saparate\git\krakatoacore-fe\src\main.ts */"zUnb");


/***/ }),

/***/ "1LmZ":
/*!**********************************************!*\
  !*** ./src/app/pages/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/button */ "jIHw");








const _c0 = function () { return { "padding": "5px!important" }; };
function HomeComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_div_9_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5); const item_r2 = ctx.$implicit; const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.showDialog(item_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c0));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("src", "assets/logos/", item_r2.appname, ".png", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r2.applabel);
} }
function HomeComponent_ng_template_14_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_ng_template_14_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r6.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_ng_template_14_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.switchApp(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function () { return { width: "20vw" }; };
class HomeComponent {
    constructor(authservice, route, sessionStorage) {
        this.authservice = authservice;
        this.route = route;
        this.sessionStorage = sessionStorage;
        this.appsByLiscense = [];
        this.display = false;
        this.userInfo = {};
        this.appIdSelected = "0";
        this.appLabelSelected = "unknown";
        this.appLabelRouteLink = "unknown";
        this.tokenID = "";
    }
    ngOnInit() {
        this.authservice.whoAmi().subscribe((value) => {
            console.log(">>> User Info : " + JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            if (value.exp) {
                this.sessionStorage.clear();
                this.authservice.setAuthStatus(false);
                this.route.navigate(['/auth/login']);
            }
            else {
                if (this.userInfo.apps.length > 0) {
                    this.appsByLiscense = this.userInfo.apps;
                }
            }
        });
    }
    showDialog(payload) {
        // this.appIdSelected = this.userInfo.leveltenant == "0"?  payload.id_application:payload.idapp;
        console.log(">>>>>>> " + JSON.stringify(payload));
        // this.appIdSelected = payload.id_application;
        if (!payload.idapp) {
            this.appIdSelected = payload.id_application;
        }
        else {
            this.appIdSelected = payload.idapp;
        }
        this.appLabelSelected = payload.applabel;
        this.appLabelRouteLink = payload.routelink;
        // console.log(">>>>>>> Payload "+this.appIdSelected);
        // this.comparentchildservice.publish('call-parent', payload);
        this.display = true;
    }
    switchApp() {
        this.display = false;
        var payloadNumber = +this.appIdSelected;
        this.authservice.changeAppLication(payloadNumber).subscribe((value) => {
            console.log(">>> User Info on switch : " + JSON.stringify(value));
            window.open(this.appLabelRouteLink + "/" + this.tokenID);
        });
        // Update dulu
        // window.open(this.appLabelRouteLink+"/"+this.tokenID);
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 15, vars: 9, consts: [[1, "wrapper"], [2, "padding-left", "40px", "padding-right", "40px"], [2, "font-weight", "400!important"], [1, "p-grid"], [1, "p-col-12"], ["class", "p-col-1", 3, "style", 4, "ngFor", "ngForOf"], [2, "height", "20px"], ["header", "Switch Application", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], [1, "p-col-1"], ["href", "javascript:void(0);", 2, "text-decoration", "none", 3, "click"], [1, "col"], [1, "row", "p-text-center"], ["alt", "Card", 2, "width", "110px", "padding", "15px 0 0 0", 3, "src"], [1, "row", "p-text-center", "btntitle"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h3", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Welcome to Krakatoa");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Choose your applications");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, HomeComponent_div_9_Template, 7, 5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p-dialog", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function HomeComponent_Template_p_dialog_visibleChange_11_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, HomeComponent_ng_template_14_Template, 2, 0, "ng-template", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.appsByLiscense);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](8, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("Open \"", ctx.appLabelSelected, "\" application?, you could log of your current login from that application.");
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], primeng_dialog__WEBPACK_IMPORTED_MODULE_5__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_6__["PrimeTemplate"], primeng_button__WEBPACK_IMPORTED_MODULE_7__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "4ei0":
/*!*******************************************************!*\
  !*** ./src/app/services/root/applications.service.ts ***!
  \*******************************************************/
/*! exports provided: ApplicationsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationsService", function() { return ApplicationsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class ApplicationsService {
    constructor(service) {
        this.service = service;
        this.allApplications = [];
        this.applications = [];
    }
    retriveAppByTenant() {
        const url = 'adm/apps/appsbytenant';
        return this.service.get(url);
    }
    retriveAppByTenantAndOrgId(id) {
        const url = `adm/apps/retriveAppByTenantAndOrgId/${id}`;
        return this.service.get(url);
    }
    clearData() {
        this.allApplications = [];
        this.applications = [];
    }
}
ApplicationsService.ɵfac = function ApplicationsService_Factory(t) { return new (t || ApplicationsService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
ApplicationsService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: ApplicationsService, factory: ApplicationsService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "4nGI":
/*!******************************************************!*\
  !*** ./src/app/services/root/usermanager.service.ts ***!
  \******************************************************/
/*! exports provided: UsermanagerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsermanagerService", function() { return UsermanagerService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class UsermanagerService {
    constructor(service) {
        this.service = service;
    }
    retriveProfile(id) {
        const url = 'adm/profile/' + id;
        return this.service.get(url);
    }
    putPassword(payload) {
        const url = 'adm/profile/changepassword';
        return this.service.put(url, payload);
    }
    insertBySuper(payload) {
        const url = 'adm/umanager/insertbysuper';
        return this.service.post(url, payload);
    }
    updatebySuper(payload) {
        const url = 'adm/umanager/updatebysupperuser';
        return this.service.post(url, payload);
    }
    retriveUsers() {
        const url = 'adm/umanager/retriveusers';
        return this.service.get(url);
    }
    retriveUsersById(id) {
        const url = 'adm/umanager/retriveusersbyid/' + id;
        return this.service.get(url);
    }
    deleteUser(payload) {
        // console.log("HAPUS "+JSON.stringify(payload));
        const url = `adm/umanager/deletebysuper/${payload.user.id} `;
        return this.service.get(url);
    }
}
UsermanagerService.ɵfac = function UsermanagerService_Factory(t) { return new (t || UsermanagerService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
UsermanagerService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: UsermanagerService, factory: UsermanagerService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "7YUa":
/*!**********************************************************************************************!*\
  !*** ./src/app/pages/root/applicationgroup/applicationdetail/applicationdetail.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: ApplicationdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationdetailComponent", function() { return ApplicationdetailComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/root/organization.service */ "u0Pv");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var src_app_services_root_applications_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/services/root/applications.service */ "4ei0");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");















function ApplicationdetailComponent_div_12_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Group Name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ApplicationdetailComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ApplicationdetailComponent_div_12_span_1_Template, 2, 0, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r0.f.organizationname.errors.required);
} }
function ApplicationdetailComponent_div_19_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Description is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ApplicationdetailComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ApplicationdetailComponent_div_19_span_1_Template, 2, 0, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.description.errors.required);
} }
function ApplicationdetailComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "span", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Choose one of licensed applications");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function ApplicationdetailComponent_ng_template_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](0, " Choose one of licensed applications ");
} }
const _c0 = function () { return { "width": "190px" }; };
function ApplicationdetailComponent_ng_template_25_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "th", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "Application Name");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "th", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Access");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](2, _c0));
} }
function ApplicationdetailComponent_ng_template_26_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "td", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "td", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "p-tableRadioButton", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const app_r8 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](app_r8.applabel);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("value", app_r8);
} }
class ApplicationdetailComponent {
    constructor(authservice, dialogService, messageService, organizationService, router, location, formBuilder, applicationsService, activatedRoute) {
        var _a;
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.organizationService = organizationService;
        this.router = router;
        this.location = location;
        this.formBuilder = formBuilder;
        this.applicationsService = applicationsService;
        this.activatedRoute = activatedRoute;
        this.extraInfo = {};
        this.isEdit = false;
        this.userInfo = {};
        this.selectedApps = [];
        this.tokenID = "";
        this.submitted = false;
        this.organizationId = '';
        this.apps = [];
        this.appNotSelected = false;
        this.extraInfo = (_a = this.router.getCurrentNavigation()) === null || _a === void 0 ? void 0 : _a.finalUrl.toString();
        let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
        console.log(">>>>>>>>>>> " + this.extraInfo);
        console.log(checkurl);
        if (checkurl)
            this.isEdit = true;
        // console.dir(this.router);
    }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Applications Group', command: (event) => {
                    this.location.back();
                }, url: "" }, { label: this.isEdit ? 'Edit data' : 'Add data' }
        ];
        this.orgForm = this.formBuilder.group({
            organizationname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
        });
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            if (!this.isEdit) {
                this.applicationsService
                    .retriveAppByTenant()
                    .subscribe((appsresult) => {
                    // console.log(">>>> Data : "+JSON.stringify(appsresult));
                    if (appsresult.data.length > 0) {
                        this.apps = appsresult.data;
                        this.applicationsService.allApplications = this.apps;
                    }
                });
            }
            else {
                // console.log(">>>>>>>>>> ID Edit "+this.activatedRoute.snapshot.paramMap.get('id'));
                if (this.activatedRoute.snapshot.paramMap.get('id')) {
                    let orgid = this.activatedRoute.snapshot.paramMap.get('id');
                    this.organizationId = orgid || '';
                    this.organizationService
                        .retriveOrgByTenantAndOrgId(orgid)
                        .subscribe((result) => {
                        const organization = result.data[0];
                        this.orgForm.patchValue({
                            organizationname: organization.orgname,
                            description: organization.orgdescription,
                        });
                    });
                    this.applicationsService
                        .retriveAppByTenant()
                        .subscribe((appsresult) => {
                        // console.log(">>>> Data : "+JSON.stringify(appsresult));
                        if (appsresult.data.length > 0) {
                            this.apps = appsresult.data;
                            this.applicationsService.allApplications = this.apps;
                            this.applicationsService
                                .retriveAppByTenantAndOrgId(orgid)
                                .subscribe((appsresult) => {
                                let selectedAppFromResult = appsresult.data;
                                var filteredApp = [];
                                // console.log("selectedAppFromResult >>>"+selectedAppFromResult);
                                if (selectedAppFromResult.length > 0) {
                                    selectedAppFromResult.map((selApp) => {
                                        this.apps.map((app) => {
                                            app.id_application === selApp.idapp
                                                ? filteredApp.push(app)
                                                : null;
                                        });
                                    });
                                    console.log(filteredApp);
                                    this.selectedApps = filteredApp;
                                    this.applicationsService.applications = this.selectedApps;
                                }
                            });
                        }
                    });
                }
            }
        });
    }
    onRowSelect(event) {
        this.applicationsService.applications = [];
        this.appNotSelected = false;
        let index = this.applicationsService.allApplications.findIndex((application) => {
            return application.id === event.data.id;
        });
        this.applicationsService.allApplications[index].selected = true;
        this.applicationsService.applications.push(event.data);
    }
    onSubmit() {
        this.submitted = true;
        // console.log('VALID ' + this.orgForm.valid);
        if (this.orgForm.valid) {
            if (this.orgForm.valid) {
                if (this.applicationsService.applications.length > 0) {
                    if (this.organizationId) {
                        // console.log(">>>> "+ JSON.stringify(this.applicationsService.applications));
                        this.appNotSelected = false;
                        let organization = {
                            id: this.organizationId,
                            name: this.orgForm.value['organizationname'],
                            description: this.orgForm.value['description'],
                        };
                        let payload = {
                            organization,
                            apps: this.applicationsService.applications,
                        };
                        console.log(payload);
                        this.organizationService
                            .editOrg(payload)
                            .subscribe((result) => {
                            if (result.status === 200) {
                                // this.router.navigate(['/administer/manageorganization']);
                                this.location.back();
                            }
                        });
                    }
                    else {
                        this.appNotSelected = false;
                        let payload = {
                            orgname: this.orgForm.value['organizationname'],
                            orgdescription: this.orgForm.value['description'],
                            apps: this.applicationsService.applications,
                        };
                        console.log('>>>> Payload. ' + JSON.stringify(payload));
                        // console.log('>>>>. ' + JSON.stringify(payload));
                        // console.log('>>>>. ' + JSON.stringify(payload));
                        this.organizationService
                            .insertOrg(payload)
                            .subscribe((result) => {
                            if (result.status === 200) {
                                // this.router.navigate(['/administer/manageorganization']);
                                this.location.back();
                            }
                        });
                    }
                }
                else {
                    this.appNotSelected = true;
                }
            }
        }
    }
    get f() {
        return this.orgForm.controls;
    }
}
ApplicationdetailComponent.ɵfac = function ApplicationdetailComponent_Factory(t) { return new (t || ApplicationdetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_3__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_4__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_5__["OrganizationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_7__["Location"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_root_applications_service__WEBPACK_IMPORTED_MODULE_8__["ApplicationsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"])); };
ApplicationdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: ApplicationdetailComponent, selectors: [["app-applicationdetail"]], decls: 32, vars: 9, consts: [[3, "home", "model"], [1, "wrapperinside"], [2, "padding", "2px", 3, "formGroup", "ngSubmit"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col-4"], [1, "box"], ["for", "organizationname"], ["name", "organizationname", "formControlName", "organizationname", "id", "organizationname", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "p-field", 4, "ngIf"], [2, "height", "15px"], ["for", "description"], [2, "height", "10px"], ["id", "description", "formControlName", "description", "rows", "3", "cols", "109", "pInputTextarea", "", 1, "p-inputtext"], [1, "p-col-8"], [3, "ngIf"], ["styleClass", "p-datatable-sm p-datatable-gridlines", 3, "value", "selection", "selectionChange", "onRowSelect"], ["pTemplate", "header"], ["pTemplate", "body"], [1, "p-field", "p-col", "p-mt-5"], [1, "p-formgrid", "p-grid", 2, "text-align", "right"], [1, "p-field", "p-col"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Save", "icon", "pi pi-angle-right", "iconPos", "right", 1, "p-button-success"], [1, "p-field"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], [2, "color", "red", "font-size", "14px"], [1, "p-text-center"], [1, "p-p-2"], [3, "value"]], template: function ApplicationdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function ApplicationdetailComponent_Template_form_ngSubmit_2_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "p-card", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "label", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "Group Name * :");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](11, "input", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, ApplicationdetailComponent_div_12_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](13, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "label", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Description *");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "textarea", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, ApplicationdetailComponent_div_19_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, ApplicationdetailComponent_div_21_Template, 3, 0, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](22, ApplicationdetailComponent_ng_template_22_Template, 1, 0, "ng-template", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "p-table", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("selectionChange", function ApplicationdetailComponent_Template_p_table_selectionChange_24_listener($event) { return ctx.selectedApps = $event; })("onRowSelect", function ApplicationdetailComponent_Template_p_table_onRowSelect_24_listener($event) { return ctx.onRowSelect($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](25, ApplicationdetailComponent_ng_template_25_Template, 5, 3, "ng-template", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](26, ApplicationdetailComponent_ng_template_26_Template, 5, 2, "ng-template", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](31, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.orgForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.organizationname.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.description.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.appNotSelected);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.appNotSelected);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("value", ctx.apps)("selection", ctx.selectedApps);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_9__["Breadcrumb"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_10__["Card"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_11__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], primeng_table__WEBPACK_IMPORTED_MODULE_12__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_4__["PrimeTemplate"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["ButtonDirective"], primeng_table__WEBPACK_IMPORTED_MODULE_12__["TableRadioButton"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBsaWNhdGlvbmRldGFpbC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "D8EZ":
/*!************************************************!*\
  !*** ./src/app/pages/login/login.component.ts ***!
  \************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");















function LoginComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "img", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function LoginComponent_div_13_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "User is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function LoginComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, LoginComponent_div_13_span_1_Template, 2, 0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r1.f.userid.errors.required);
} }
function LoginComponent_div_19_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Password is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function LoginComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, LoginComponent_div_19_span_1_Template, 2, 0, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx_r2.f.secret.errors.required);
} }
function LoginComponent_ng_template_23_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", ctx_r3.isProcess);
} }
const _c0 = function () { return { width: "360px" }; };
class LoginComponent {
    constructor(messageService, sessionStorage, route, formBuilder, backend, authservice) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.blockedDocument = false;
        //  userid = "";
        this.secret = "";
        this.errorMsg = "";
        this.isProcess = false;
        this.submitted = false;
    }
    ngOnInit() {
        this.userForm = this.formBuilder.group({
            "userid": ["", _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
            "secret": ["", _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required],
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.userForm.controls; }
    get userFormControl() {
        return this.userForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        if (this.userForm.valid) {
            this.isProcess = true;
            this.backend.post('adm/auth/signviakkt', { credential: this.userForm.value["userid"], secret: this.userForm.value["secret"] }, false).subscribe((data) => {
                console.log(">>>>>>> Ang Dari Server " + JSON.stringify(data));
                if (data.status === 200) {
                    const authToken = data.data;
                    this.sessionStorage.set("accesstoken", authToken);
                    this.authservice.setAuthStatus(true);
                    this.authservice.setToken(authToken);
                    this.authservice.whoAmi().subscribe((data) => {
                        console.log(">>>>>>> " + JSON.stringify(data));
                        if (data.status = 200) {
                            let objecInfo = data.data;
                            let lvlTn = parseInt(objecInfo.leveltenant);
                            console.log(">> LVL TENANT >> " + lvlTn);
                            if (objecInfo.leveltenant < 1) {
                                this.route.navigate(['/mgm/root']);
                            }
                            else {
                                this.route.navigate(['/mgm/admin']);
                            }
                        }
                    });
                    this.isProcess = false;
                }
                else {
                    this.sessionStorage.clear();
                    this.showTopCenterErr("Invalid user and password");
                    this.authservice.setAuthStatus(false);
                    this.isProcess = false;
                }
            }, error => {
                this.showTopCenterErr("Invalid user and password");
                this.isProcess = false;
                this.authservice.setAuthStatus(false);
            });
        }
        //  this.blockDocument();
    }
    showTopCenterErr(message) {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: message });
    }
    blockDocument() {
        this.blockedDocument = true;
        //  setTimeout(() => {
        //      this.blockedDocument = false;
        //      this.showTopCenterErr("Invalid user and password!")
        //  }, 3000);
    }
}
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_5__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_6__["AuthService"])); };
LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: LoginComponent, selectors: [["app-login"]], decls: 25, vars: 7, consts: [[3, "target", "blocked"], [2, "color", "antiquewhite", "font-size", "36px"], [1, "form", 3, "formGroup", "ngSubmit"], ["header", "One Solutions", "styleClass", "p-card-shadow p-header-w50"], ["pTemplate", "header"], [1, "p-fluid"], [1, "p-field"], ["for", "userid", 1, "labelpb"], ["id", "userid", "name", "userid", "type", "text", "required", "", "formControlName", "userid", "pInputText", ""], ["class", "p-field", 4, "ngIf"], [2, "height", "0.3rem"], ["for", "secret", 1, "labelpb"], ["id", "secret", "type", "password", "formControlName", "secret", "pInputText", "", 3, "keyup.enter"], ["href", "/auth/forgotpassword"], ["pTemplate", "footer"], [1, "p-text-center"], ["alt", "tai", "src", "assets/logos/krakatoalogo.png"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], [1, "p-text-right"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Login", 1, "p-primary-btn", 3, "disabled"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-blockUI", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-progressSpinner");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h1", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Loading");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngSubmit", function LoginComponent_Template_form_ngSubmit_5_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "p-card", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, LoginComponent_ng_template_7_Template, 2, 0, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "label", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "User Name");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](13, LoginComponent_div_13_Template, 2, 1, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "label", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("keyup.enter", function LoginComponent_Template_input_keyup_enter_18_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](19, LoginComponent_div_19_Template, 2, 1, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, "Forgot Password?");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](23, LoginComponent_ng_template_23_Template, 2, 1, "ng-template", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](24, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.isProcess);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.userForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](6, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.userid.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.secret.errors);
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_7__["BlockUI"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_8__["ProgressSpinner"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_9__["Card"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__["InputText"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_12__["Messages"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["ButtonDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuc2NzcyJ9 */", "body[_ngcontent-%COMP%] {\n        background: #007dc5\n    }\n\n    section[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 40%;\n        left: 50%;\n        margin-right: -50%;\n        transform: translate(-50%, -50%)\n    }"] });


/***/ }),

/***/ "GK+Y":
/*!********************************************************!*\
  !*** ./src/app/pages/homeadmin/homeadmin.component.ts ***!
  \********************************************************/
/*! exports provided: HomeadminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeadminComponent", function() { return HomeadminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_divider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/divider */ "lUkA");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var primeng_chart__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/chart */ "I5S5");










class HomeadminComponent {
    constructor(authservice, sessionStorage, route) {
        this.authservice = authservice;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.userInfo = {};
        this.applicationAvailable = 0;
        this.tokenID = "";
    }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.typeStrings = [{ id: "1", label: "In a Month" }, { id: "2", label: "In a Year" }];
        this.selectedType = { id: "1", label: "In a Month" };
        this.authservice.whoAmi().subscribe((value) => {
            console.log(">>> User Info : " + JSON.stringify(value));
            if (value.exp) {
                this.sessionStorage.clear();
                this.authservice.setAuthStatus(false);
                this.route.navigate(['/auth/login']);
            }
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.applicationAvailable = this.userInfo.appscount - 1;
        });
        this.basicData = {
            labels: [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            datasets: [
                {
                    label: 'Logged In Users',
                    data: [5, 2, 1, 0, 9, 10, 1, 2, 10, 0, 1, 7, 7, 8, 1, 0, 2, 2, 3],
                    fill: false,
                    borderColor: '#42A5F5',
                    tension: .4
                },
            ]
        };
    }
}
HomeadminComponent.ɵfac = function HomeadminComponent_Factory(t) { return new (t || HomeadminComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_2__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"])); };
HomeadminComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeadminComponent, selectors: [["app-homeadmin"]], decls: 71, vars: 7, consts: [[2, "height", "20px"], [1, "wrapper"], [1, "p-grid"], [1, "p-col-3"], [1, "box"], ["header", "Available Application", "styleClass", "p-card-shadow p-cst-primary"], [1, "p-col", "p-text-center"], [1, "pi", "pi-desktop", "p-pt-15", 2, "font-size", "3.5rem!important"], ["layout", "vertical"], [1, "p-col", "p-text-center", "p-pt-15"], [2, "font-size", "3.5rem!important", "padding-top", "15px"], ["header", "Application Groups", "styleClass", "p-card-shadow p-cst-primary"], [1, "pi", "pi-th-large", "p-pt-15", 2, "font-size", "3.5rem!important"], ["header", "Current Users Activity", "styleClass", "p-card-shadow p-cst-primary"], [1, "p-col-3", "p-text-start"], [2, "font-size", "0.8rem", "padding", "0!important"], [1, "p-col-2", "p-text-start"], [1, "p-col-7", "p-text-right"], [1, "p-col-3", "p-text-start", "p-pt-1"], [1, "p-col-2", "p-text-start", "p-pt-1"], [1, "p-col-7", "p-text-right", "p-pt-1"], [2, "font-size", "0.8rem", "font-weight", "600", "padding", "0!important"], ["header", "Blocked Users", "styleClass", "p-card-shadow p-cst-primary"], [1, "pi", "pi-lock", "p-pt-15", 2, "font-size", "3.5rem!important"], [1, "p-toolbar-group-left", 2, "padding-left", "10px"], [2, "margin", "10px 1px"], [1, "p-toolbar-group-right", 2, "padding-right", "20px"], ["optionLabel", "label", 3, "options", "ngModel", "showClear", "ngModelChange"], ["type", "line", 3, "data", "options", "height"]], template: function HomeadminComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p-card", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "i", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "p-divider", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p-card", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "i", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "p-divider", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "0");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p-card", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Active");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "10 Users");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Pasive");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "p", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "0 Users");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Total");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, ":");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "10 Users");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "p-card", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "i", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "p-divider", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "0");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "h3", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Log frequencies");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "p-dropdown", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function HomeadminComponent_Template_p_dropdown_ngModelChange_69_listener($event) { return ctx.selectedType = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](70, "p-chart", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.applicationAvailable);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx.typeStrings)("ngModel", ctx.selectedType)("showClear", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.basicData)("options", ctx.basicOptions)("height", 70);
    } }, directives: [primeng_card__WEBPACK_IMPORTED_MODULE_4__["Card"], primeng_divider__WEBPACK_IMPORTED_MODULE_5__["Divider"], primeng_toolbar__WEBPACK_IMPORTED_MODULE_6__["Toolbar"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__["Dropdown"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], primeng_chart__WEBPACK_IMPORTED_MODULE_9__["UIChart"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lYWRtaW4uY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "J4FQ":
/*!*************************************************************!*\
  !*** ./src/app/pages/root/eventlogs/eventlogs.component.ts ***!
  \*************************************************************/
/*! exports provided: EventlogsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventlogsComponent", function() { return EventlogsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class EventlogsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Eventlogs Monitoring' }
        ];
    }
}
EventlogsComponent.ɵfac = function EventlogsComponent_Factory(t) { return new (t || EventlogsComponent)(); };
EventlogsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: EventlogsComponent, selectors: [["app-eventlogs"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function EventlogsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJldmVudGxvZ3MuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "MUFR":
/*!*******************************************************************!*\
  !*** ./src/app/layout/fullmenulayout/fullmenulayout.component.ts ***!
  \*******************************************************************/
/*! exports provided: FullmenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FullmenulayoutComponent", function() { return FullmenulayoutComponent; });
/* harmony import */ var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/_files/appadmin.json */ "xlh6");
var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! src/app/_files/appadmin.json */ "xlh6", 1);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/menubar */ "b1Ni");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");














function FullmenulayoutComponent_ng_template_29_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FullmenulayoutComponent_ng_template_29_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r1.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-button", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function FullmenulayoutComponent_ng_template_29_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r2); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r3.signOut(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
class FullmenulayoutComponent {
    constructor(messageService, sessionStorage, route, formBuilder, backend, authservice, router) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.route = route;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.router = router;
        this.username = "Unknown";
        this.companytitle = "Unknown";
        this.display = false;
        this.isProcess = false;
        // authToken = "";
        this.jsonUserInfo = {};
        this.title = 'krakatoa';
    }
    ngOnInit() {
        console.log("###########APP COMPONENT Admin #############");
        this.isProcess = true;
        this.authservice.whoAmi().subscribe((value) => {
            this.jsonUserInfo = value;
            this.username = this.jsonUserInfo.data.fullname;
            this.companytitle = this.jsonUserInfo.data.tnname;
            this.items = [
                {
                    label: this.username,
                    icon: 'pi pi-fw pi-user',
                    items: [
                        {
                            label: 'My Account',
                            icon: 'pi pi-user-edit',
                        },
                        {
                            label: '',
                            separator: true,
                        },
                        {
                            label: 'Sign out',
                            icon: 'pi pi-fw pi-sign-out',
                            command: () => {
                                // this.delete();
                                this.showDialog();
                            }
                        }
                    ]
                },
                {
                    label: 'Help',
                    icon: 'pi pi-fw pi-info-circle',
                },
            ];
            let lvlTn = parseInt(this.jsonUserInfo.data.leveltenant);
            console.log(">> Level tenant " + this.jsonUserInfo.data.leveltenant);
            if (lvlTn == 0) {
                this.sidemenus = src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__;
            }
            this.isProcess = false;
        });
    }
    showDialog() {
        this.display = true;
    }
    signOut() {
        this.display = false;
        this.isProcess = true;
        let payload = { appid: 1, appname: "Krakatoa" };
        console.log("Ini harusnya keluar >> " + JSON.stringify(payload));
        // let payload = {appid:1, appname: "Krakatoa"};
        this.backend.post('adm/auth/signout', payload, false).subscribe((data) => {
            // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
            this.isProcess = false;
            if (data.status === 200) {
                this.authservice.loggedOut();
                this.router.navigate(['/auth/login']);
            }
            else {
            }
        }, error => {
        });
        // this.isProcess= true
    }
}
FullmenulayoutComponent.ɵfac = function FullmenulayoutComponent_Factory(t) { return new (t || FullmenulayoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
FullmenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: FullmenulayoutComponent, selectors: [["app-fullmenulayout"]], decls: 30, vars: 10, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], [3, "blocked"], [2, "color", "antiquewhite", "font-size", "36px"], ["id", "wrapper"], ["id", "contentliquid2"], ["id", "contentfull"], [1, "p-toolbar-group-left", 2, "padding-left", "10px"], [1, "p-pt-15", "p-pb-15", "forcecenter"], ["src", "assets/logos/krakatoalogo.png", "height", "17.5", 2, "padding-left", "40px", "padding-right", "40px"], [2, "margin", "0"], [2, "color", "#08736f"], [1, "p-toolbar-group-right"], [3, "model"], ["header", "Sign Out", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function FullmenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Document");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "p-blockUI", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "p-progressSpinner");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "h1", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Loading");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "img", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "h3", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "One Solution of ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](24, "p-menubar", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](25, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "p-dialog", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function FullmenulayoutComponent_Template_p_dialog_visibleChange_26_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "p", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, "Sign out from application?, if yes you should log in again from the beginning");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](29, FullmenulayoutComponent_ng_template_29_Template, 2, 0, "ng-template", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("blocked", ctx.isProcess);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.companytitle, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("model", ctx.items);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](9, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_blockui__WEBPACK_IMPORTED_MODULE_8__["BlockUI"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__["ProgressSpinner"], primeng_toolbar__WEBPACK_IMPORTED_MODULE_10__["Toolbar"], primeng_menubar__WEBPACK_IMPORTED_MODULE_11__["Menubar"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterOutlet"], primeng_dialog__WEBPACK_IMPORTED_MODULE_12__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmdWxsbWVudWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "QdSJ":
/*!******************************************************************!*\
  !*** ./src/app/pages/forgotpassword/forgotpassword.component.ts ***!
  \******************************************************************/
/*! exports provided: ForgotpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgotpasswordComponent", function() { return ForgotpasswordComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class ForgotpasswordComponent {
    constructor() { }
    ngOnInit() {
    }
}
ForgotpasswordComponent.ɵfac = function ForgotpasswordComponent_Factory(t) { return new (t || ForgotpasswordComponent)(); };
ForgotpasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ForgotpasswordComponent, selectors: [["app-forgotpassword"]], decls: 2, vars: 0, template: function ForgotpasswordComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "forgotpassword works!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb3Jnb3RwYXNzd29yZC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "RnhZ":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "K/tc",
	"./af.js": "K/tc",
	"./ar": "jnO4",
	"./ar-dz": "o1bE",
	"./ar-dz.js": "o1bE",
	"./ar-kw": "Qj4J",
	"./ar-kw.js": "Qj4J",
	"./ar-ly": "HP3h",
	"./ar-ly.js": "HP3h",
	"./ar-ma": "CoRJ",
	"./ar-ma.js": "CoRJ",
	"./ar-sa": "gjCT",
	"./ar-sa.js": "gjCT",
	"./ar-tn": "bYM6",
	"./ar-tn.js": "bYM6",
	"./ar.js": "jnO4",
	"./az": "SFxW",
	"./az.js": "SFxW",
	"./be": "H8ED",
	"./be.js": "H8ED",
	"./bg": "hKrs",
	"./bg.js": "hKrs",
	"./bm": "p/rL",
	"./bm.js": "p/rL",
	"./bn": "kEOa",
	"./bn-bd": "loYQ",
	"./bn-bd.js": "loYQ",
	"./bn.js": "kEOa",
	"./bo": "0mo+",
	"./bo.js": "0mo+",
	"./br": "aIdf",
	"./br.js": "aIdf",
	"./bs": "JVSJ",
	"./bs.js": "JVSJ",
	"./ca": "1xZ4",
	"./ca.js": "1xZ4",
	"./cs": "PA2r",
	"./cs.js": "PA2r",
	"./cv": "A+xa",
	"./cv.js": "A+xa",
	"./cy": "l5ep",
	"./cy.js": "l5ep",
	"./da": "DxQv",
	"./da.js": "DxQv",
	"./de": "tGlX",
	"./de-at": "s+uk",
	"./de-at.js": "s+uk",
	"./de-ch": "u3GI",
	"./de-ch.js": "u3GI",
	"./de.js": "tGlX",
	"./dv": "WYrj",
	"./dv.js": "WYrj",
	"./el": "jUeY",
	"./el.js": "jUeY",
	"./en-au": "Dmvi",
	"./en-au.js": "Dmvi",
	"./en-ca": "OIYi",
	"./en-ca.js": "OIYi",
	"./en-gb": "Oaa7",
	"./en-gb.js": "Oaa7",
	"./en-ie": "4dOw",
	"./en-ie.js": "4dOw",
	"./en-il": "czMo",
	"./en-il.js": "czMo",
	"./en-in": "7C5Q",
	"./en-in.js": "7C5Q",
	"./en-nz": "b1Dy",
	"./en-nz.js": "b1Dy",
	"./en-sg": "t+mt",
	"./en-sg.js": "t+mt",
	"./eo": "Zduo",
	"./eo.js": "Zduo",
	"./es": "iYuL",
	"./es-do": "CjzT",
	"./es-do.js": "CjzT",
	"./es-mx": "tbfe",
	"./es-mx.js": "tbfe",
	"./es-us": "Vclq",
	"./es-us.js": "Vclq",
	"./es.js": "iYuL",
	"./et": "7BjC",
	"./et.js": "7BjC",
	"./eu": "D/JM",
	"./eu.js": "D/JM",
	"./fa": "jfSC",
	"./fa.js": "jfSC",
	"./fi": "gekB",
	"./fi.js": "gekB",
	"./fil": "1ppg",
	"./fil.js": "1ppg",
	"./fo": "ByF4",
	"./fo.js": "ByF4",
	"./fr": "nyYc",
	"./fr-ca": "2fjn",
	"./fr-ca.js": "2fjn",
	"./fr-ch": "Dkky",
	"./fr-ch.js": "Dkky",
	"./fr.js": "nyYc",
	"./fy": "cRix",
	"./fy.js": "cRix",
	"./ga": "USCx",
	"./ga.js": "USCx",
	"./gd": "9rRi",
	"./gd.js": "9rRi",
	"./gl": "iEDd",
	"./gl.js": "iEDd",
	"./gom-deva": "qvJo",
	"./gom-deva.js": "qvJo",
	"./gom-latn": "DKr+",
	"./gom-latn.js": "DKr+",
	"./gu": "4MV3",
	"./gu.js": "4MV3",
	"./he": "x6pH",
	"./he.js": "x6pH",
	"./hi": "3E1r",
	"./hi.js": "3E1r",
	"./hr": "S6ln",
	"./hr.js": "S6ln",
	"./hu": "WxRl",
	"./hu.js": "WxRl",
	"./hy-am": "1rYy",
	"./hy-am.js": "1rYy",
	"./id": "UDhR",
	"./id.js": "UDhR",
	"./is": "BVg3",
	"./is.js": "BVg3",
	"./it": "bpih",
	"./it-ch": "bxKX",
	"./it-ch.js": "bxKX",
	"./it.js": "bpih",
	"./ja": "B55N",
	"./ja.js": "B55N",
	"./jv": "tUCv",
	"./jv.js": "tUCv",
	"./ka": "IBtZ",
	"./ka.js": "IBtZ",
	"./kk": "bXm7",
	"./kk.js": "bXm7",
	"./km": "6B0Y",
	"./km.js": "6B0Y",
	"./kn": "PpIw",
	"./kn.js": "PpIw",
	"./ko": "Ivi+",
	"./ko.js": "Ivi+",
	"./ku": "JCF/",
	"./ku.js": "JCF/",
	"./ky": "lgnt",
	"./ky.js": "lgnt",
	"./lb": "RAwQ",
	"./lb.js": "RAwQ",
	"./lo": "sp3z",
	"./lo.js": "sp3z",
	"./lt": "JvlW",
	"./lt.js": "JvlW",
	"./lv": "uXwI",
	"./lv.js": "uXwI",
	"./me": "KTz0",
	"./me.js": "KTz0",
	"./mi": "aIsn",
	"./mi.js": "aIsn",
	"./mk": "aQkU",
	"./mk.js": "aQkU",
	"./ml": "AvvY",
	"./ml.js": "AvvY",
	"./mn": "lYtQ",
	"./mn.js": "lYtQ",
	"./mr": "Ob0Z",
	"./mr.js": "Ob0Z",
	"./ms": "6+QB",
	"./ms-my": "ZAMP",
	"./ms-my.js": "ZAMP",
	"./ms.js": "6+QB",
	"./mt": "G0Uy",
	"./mt.js": "G0Uy",
	"./my": "honF",
	"./my.js": "honF",
	"./nb": "bOMt",
	"./nb.js": "bOMt",
	"./ne": "OjkT",
	"./ne.js": "OjkT",
	"./nl": "+s0g",
	"./nl-be": "2ykv",
	"./nl-be.js": "2ykv",
	"./nl.js": "+s0g",
	"./nn": "uEye",
	"./nn.js": "uEye",
	"./oc-lnc": "Fnuy",
	"./oc-lnc.js": "Fnuy",
	"./pa-in": "8/+R",
	"./pa-in.js": "8/+R",
	"./pl": "jVdC",
	"./pl.js": "jVdC",
	"./pt": "8mBD",
	"./pt-br": "0tRk",
	"./pt-br.js": "0tRk",
	"./pt.js": "8mBD",
	"./ro": "lyxo",
	"./ro.js": "lyxo",
	"./ru": "lXzo",
	"./ru.js": "lXzo",
	"./sd": "Z4QM",
	"./sd.js": "Z4QM",
	"./se": "//9w",
	"./se.js": "//9w",
	"./si": "7aV9",
	"./si.js": "7aV9",
	"./sk": "e+ae",
	"./sk.js": "e+ae",
	"./sl": "gVVK",
	"./sl.js": "gVVK",
	"./sq": "yPMs",
	"./sq.js": "yPMs",
	"./sr": "zx6S",
	"./sr-cyrl": "E+lV",
	"./sr-cyrl.js": "E+lV",
	"./sr.js": "zx6S",
	"./ss": "Ur1D",
	"./ss.js": "Ur1D",
	"./sv": "X709",
	"./sv.js": "X709",
	"./sw": "dNwA",
	"./sw.js": "dNwA",
	"./ta": "PeUW",
	"./ta.js": "PeUW",
	"./te": "XLvN",
	"./te.js": "XLvN",
	"./tet": "V2x9",
	"./tet.js": "V2x9",
	"./tg": "Oxv6",
	"./tg.js": "Oxv6",
	"./th": "EOgW",
	"./th.js": "EOgW",
	"./tk": "Wv91",
	"./tk.js": "Wv91",
	"./tl-ph": "Dzi0",
	"./tl-ph.js": "Dzi0",
	"./tlh": "z3Vd",
	"./tlh.js": "z3Vd",
	"./tr": "DoHr",
	"./tr.js": "DoHr",
	"./tzl": "z1FC",
	"./tzl.js": "z1FC",
	"./tzm": "wQk9",
	"./tzm-latn": "tT3J",
	"./tzm-latn.js": "tT3J",
	"./tzm.js": "wQk9",
	"./ug-cn": "YRex",
	"./ug-cn.js": "YRex",
	"./uk": "raLr",
	"./uk.js": "raLr",
	"./ur": "UpQW",
	"./ur.js": "UpQW",
	"./uz": "Loxo",
	"./uz-latn": "AQ68",
	"./uz-latn.js": "AQ68",
	"./uz.js": "Loxo",
	"./vi": "KSF8",
	"./vi.js": "KSF8",
	"./x-pseudo": "/X5v",
	"./x-pseudo.js": "/X5v",
	"./yo": "fzPg",
	"./yo.js": "fzPg",
	"./zh-cn": "XDpg",
	"./zh-cn.js": "XDpg",
	"./zh-hk": "SatO",
	"./zh-hk.js": "SatO",
	"./zh-mo": "OmwH",
	"./zh-mo.js": "OmwH",
	"./zh-tw": "kOpN",
	"./zh-tw.js": "kOpN"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "RnhZ";

/***/ }),

/***/ "SuKp":
/*!*******************************************************************!*\
  !*** ./src/app/layout/mainmenulayout/mainmenulayout.component.ts ***!
  \*******************************************************************/
/*! exports provided: MainmenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainmenulayoutComponent", function() { return MainmenulayoutComponent; });
/* harmony import */ var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/_files/appadmin.json */ "xlh6");
var src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! src/app/_files/appadmin.json */ "xlh6", 1);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/backend.service */ "cygB");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/menubar */ "b1Ni");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/panelmenu */ "kSmT");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/button */ "jIHw");













function MainmenulayoutComponent_ng_template_29_Template(rf, ctx) { if (rf & 1) {
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "p-button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MainmenulayoutComponent_ng_template_29_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r2); const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r1.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "p-button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MainmenulayoutComponent_ng_template_29_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r2); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r3.signOut(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { "width": "199px" }; };
const _c1 = function () { return { width: "20vw" }; };
class MainmenulayoutComponent {
    constructor(messageService, sessionStorage, routeA, route, formBuilder, backend, authservice, router) {
        this.messageService = messageService;
        this.sessionStorage = sessionStorage;
        this.routeA = routeA;
        this.route = route;
        this.formBuilder = formBuilder;
        this.backend = backend;
        this.authservice = authservice;
        this.router = router;
        this.title = "Unknown";
        this.username = "Unknown";
        this.companytitle = "Unknown";
        this.display = false;
        this.isProcess = false;
        // authToken = "";
        this.jsonUserInfo = {};
    }
    ngOnInit() {
        console.log("###########APP COMPONENT#############");
        this.title = this.routeA.snapshot.data['title'];
        this.isProcess = true;
        this.authservice.whoAmi().subscribe((value) => {
            this.jsonUserInfo = value;
            // console.log(">> INSIDE Main Menu "+JSON.stringify(this.jsonUserInfo));
            // INSIDE Main Menu {"status":200,"data":{"id":"1","fullname":"KKTCIMB","userid":"kktcimb@cimb.co.id","idtenant":"1","idsubtenant":"0","leveltenant":"0","tnname":"PT Bank CIMB","tnstatus":1,"tnflag":1,"tnparentid":0,"cdtenant":"9624","bioemailactive":"kktcimb@cimb.co.id","biophoneactive":"5554444","bioaddress":"Kantor Pusat","bionik":"3400342324342","bionpwp":null,"bioidtipenik":1,"bioidcorel":3,"orgid":0,"appscount":3,"apps":[{"id":"1","expiredate":"2022-05-23","id_application":"1","paidstatus":"1","active":1,"defaultactive":null,"appname":"default","applabel":"Default","description":"Krakatoa Administer"},{"id":"2","expiredate":"2022-05-24","id_application":"2","paidstatus":"1","active":1,"defaultactive":null,"appname":"vam","applabel":"Virtual account manager","description":"This application for maintained and monitoring Virtual Account Management purposed. And this is general operation form Banking Needed "},{"id":"3","expiredate":"2021-07-13","id_application":"3","paidstatus":"1","active":1,"defaultactive":null,"appname":"crm","applabel":"Bank CRM","description":"This contain all modules for Customer Relationship Management purpose on front end banking, and it can be customed as the users needed"}],"sidemenus":[],"iat":1629743103}}
            this.username = this.jsonUserInfo.data.fullname;
            this.companytitle = this.jsonUserInfo.data.tnname;
            this.items = [
                {
                    label: this.username,
                    icon: 'pi pi-fw pi-user',
                    items: [
                        {
                            label: 'My Account',
                            icon: 'pi pi-user-edit',
                        },
                        {
                            label: '',
                            separator: true,
                        },
                        {
                            label: 'Sign out',
                            icon: 'pi pi-fw pi-sign-out',
                            command: () => {
                                // this.delete();
                                this.showDialog();
                            }
                        }
                    ]
                },
                {
                    label: 'Help',
                    icon: 'pi pi-fw pi-info-circle',
                },
            ];
            let lvlTn = parseInt(this.jsonUserInfo.data.leveltenant);
            // console.log(">> Level tenant "+this.jsonUserInfo.data.leveltenant);
            if (lvlTn == 0) {
                this.sidemenus = src_app_files_appadmin_json__WEBPACK_IMPORTED_MODULE_0__;
            }
            this.isProcess = false;
        });
    }
    showDialog() {
        this.display = true;
    }
    signOut() {
        this.display = false;
        this.isProcess = true;
        let payload = { appid: 0, appname: "Krakatoa" };
        console.log("Ini harusnya keluar >> " + JSON.stringify(payload));
        this.backend.post('adm/auth/signout', payload, false).subscribe((data) => {
            // console.log(">>>>>>> Ang Dari Server "+JSON.stringify(data));
            this.isProcess = false;
            if (data.status === 200) {
                this.authservice.loggedOut();
                this.router.navigate(['/auth/login']);
            }
            else {
            }
        }, error => {
        });
        // this.isProcess= true
    }
}
MainmenulayoutComponent.ɵfac = function MainmenulayoutComponent_Factory(t) { return new (t || MainmenulayoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_2__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_backend_service__WEBPACK_IMPORTED_MODULE_6__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_7__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
MainmenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: MainmenulayoutComponent, selectors: [["app-mainmenulayout"]], decls: 30, vars: 14, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], ["id", "wrapper"], ["id", "contentliquid"], ["id", "content"], [1, "p-toolbar-group-left", 2, "padding-left", "10px"], [2, "margin", "0"], [2, "color", "#08736f"], [1, "p-toolbar-group-right"], [3, "model"], ["id", "leftcolumn", 1, "full-height"], [1, "p-pt-15", "p-pb-15", "forcecenter"], ["src", "assets/logos/krakatoalogo.png", "height", "17.5", 2, "padding-left", "40px"], ["header", "Sign Out", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function MainmenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "h3", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "span", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "One Solution of ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](18, "p-menubar", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](19, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](21, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](23, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](25, "p-panelMenu", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "p-dialog", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("visibleChange", function MainmenulayoutComponent_Template_p_dialog_visibleChange_26_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "p", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, "Sign out from application?, if yes you should log in again from the beginning");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](29, MainmenulayoutComponent_ng_template_29_Template, 2, 0, "ng-template", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.companytitle, "");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("model", ctx.items);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](12, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("model", ctx.sidemenus);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](13, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__["Toolbar"], primeng_menubar__WEBPACK_IMPORTED_MODULE_9__["Menubar"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterOutlet"], primeng_panelmenu__WEBPACK_IMPORTED_MODULE_10__["PanelMenu"], primeng_dialog__WEBPACK_IMPORTED_MODULE_11__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], primeng_button__WEBPACK_IMPORTED_MODULE_12__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJtYWlubWVudWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");


class AppComponent {
    ngOnInit() {
        console.log("###########APP COMPONENT#############");
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "WQ6N":
/*!***************************************************************!*\
  !*** ./src/app/layout/nomenulayout/nomenulayout.component.ts ***!
  \***************************************************************/
/*! exports provided: NomenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NomenulayoutComponent", function() { return NomenulayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");


class NomenulayoutComponent {
    constructor(routeA) {
        this.routeA = routeA;
        this.title = "Unknown";
    }
    ngOnInit() {
        this.title = this.routeA.snapshot.data['title'];
        console;
    }
}
NomenulayoutComponent.ɵfac = function NomenulayoutComponent_Factory(t) { return new (t || NomenulayoutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"])); };
NomenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NomenulayoutComponent, selectors: [["app-nomenulayout"]], decls: 10, vars: 1, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], ["id", "wrapper"]], template: function NomenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.title);
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJub21lbnVsYXlvdXQuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_splitbutton__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/splitbutton */ "Wq6t");
/* harmony import */ var primeng_menubar__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/menubar */ "b1Ni");
/* harmony import */ var primeng_panelmenu__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/panelmenu */ "kSmT");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/panel */ "7CaW");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_divider__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! primeng/divider */ "lUkA");
/* harmony import */ var _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./layout/mainmenulayout/mainmenulayout.component */ "SuKp");
/* harmony import */ var _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./layout/nomenulayout/nomenulayout.component */ "WQ6N");
/* harmony import */ var _pages_homeadmin_homeadmin_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./pages/homeadmin/homeadmin.component */ "GK+Y");
/* harmony import */ var _pages_home_home_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./pages/home/home.component */ "1LmZ");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
/* harmony import */ var primeng_blockui__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! primeng/blockui */ "0LTx");
/* harmony import */ var primeng_selectbutton__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! primeng/selectbutton */ "5o1E");
/* harmony import */ var primeng_steps__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! primeng/steps */ "KcHJ");
/* harmony import */ var primeng_inputnumber__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! primeng/inputnumber */ "Ks7X");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! primeng/autocomplete */ "V5BG");
/* harmony import */ var primeng_chart__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! primeng/chart */ "I5S5");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_message__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! primeng/message */ "FMpt");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationgroup.component */ "sBLG");
/* harmony import */ var _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./pages/root/users/users.component */ "brkH");
/* harmony import */ var _pages_root_applications_applications_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./pages/root/applications/applications.component */ "hwT4");
/* harmony import */ var _pages_root_smtpaccounts_smtpaccounts_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./pages/root/smtpaccounts/smtpaccounts.component */ "qUOm");
/* harmony import */ var _pages_root_oauthsettings_oauthsettings_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./pages/root/oauthsettings/oauthsettings.component */ "brE3");
/* harmony import */ var _pages_root_eventlogs_eventlogs_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./pages/root/eventlogs/eventlogs.component */ "J4FQ");
/* harmony import */ var _pages_root_resourceusage_resourceusage_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./pages/root/resourceusage/resourceusage.component */ "q50L");
/* harmony import */ var _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./pages/errorpage/errorpage.component */ "bzlq");
/* harmony import */ var _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./layout/backmenulayout/backmenulayout.component */ "nMyi");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./pages/login/login.component */ "D8EZ");
/* harmony import */ var _pages_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./pages/forgotpassword/forgotpassword.component */ "QdSJ");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _interceptors_interceptor_http_service__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ./interceptors/interceptor-http.service */ "mnfZ");
/* harmony import */ var _layout_fullmenulayout_fullmenulayout_component__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! ./layout/fullmenulayout/fullmenulayout.component */ "MUFR");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(/*! ./generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationdetail/applicationdetail.component */ "7YUa");
/* harmony import */ var _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(/*! ./pages/root/users/userdetail/userdetail.component */ "z+Ab");
/* harmony import */ var _env_env_service_provider__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(/*! ./env/env.service.provider */ "mPzt");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(/*! @angular/core */ "fXoL");





















































class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_52__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_52__["ɵɵdefineInjector"]({ providers: [
        _env_env_service_provider__WEBPACK_IMPORTED_MODULE_51__["EnvServiceProvider"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_16__["DynamicDialogRef"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_16__["DynamicDialogConfig"],
        primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"],
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_45__["HTTP_INTERCEPTORS"],
            useClass: _interceptors_interceptor_http_service__WEBPACK_IMPORTED_MODULE_46__["InterceptorHttpService"],
            multi: true,
        },
        { provide: _angular_common__WEBPACK_IMPORTED_MODULE_4__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_4__["HashLocationStrategy"] },
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_16__["DialogService"],
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_45__["HttpClientModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            primeng_card__WEBPACK_IMPORTED_MODULE_7__["CardModule"],
            primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__["ToolbarModule"],
            primeng_button__WEBPACK_IMPORTED_MODULE_10__["ButtonModule"],
            primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__["InputTextModule"],
            primeng_splitbutton__WEBPACK_IMPORTED_MODULE_11__["SplitButtonModule"],
            primeng_menubar__WEBPACK_IMPORTED_MODULE_12__["MenubarModule"],
            primeng_panelmenu__WEBPACK_IMPORTED_MODULE_13__["PanelMenuModule"],
            primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_22__["BreadcrumbModule"],
            primeng_dropdown__WEBPACK_IMPORTED_MODULE_23__["DropdownModule"],
            primeng_chart__WEBPACK_IMPORTED_MODULE_29__["ChartModule"],
            primeng_blockui__WEBPACK_IMPORTED_MODULE_24__["BlockUIModule"],
            primeng_divider__WEBPACK_IMPORTED_MODULE_17__["DividerModule"],
            primeng_progressspinner__WEBPACK_IMPORTED_MODULE_33__["ProgressSpinnerModule"],
            primeng_messages__WEBPACK_IMPORTED_MODULE_31__["MessagesModule"],
            primeng_message__WEBPACK_IMPORTED_MODULE_32__["MessageModule"],
            primeng_dialog__WEBPACK_IMPORTED_MODULE_14__["DialogModule"],
            primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_16__["DynamicDialogModule"],
            primeng_panel__WEBPACK_IMPORTED_MODULE_15__["PanelModule"],
            primeng_table__WEBPACK_IMPORTED_MODULE_30__["TableModule"],
            primeng_selectbutton__WEBPACK_IMPORTED_MODULE_25__["SelectButtonModule"],
            primeng_steps__WEBPACK_IMPORTED_MODULE_26__["StepsModule"],
            primeng_inputnumber__WEBPACK_IMPORTED_MODULE_27__["InputNumberModule"],
            primeng_autocomplete__WEBPACK_IMPORTED_MODULE_28__["AutoCompleteModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_52__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
        _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_18__["MainmenulayoutComponent"],
        _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_19__["NomenulayoutComponent"],
        _pages_homeadmin_homeadmin_component__WEBPACK_IMPORTED_MODULE_20__["HomeadminComponent"],
        _pages_home_home_component__WEBPACK_IMPORTED_MODULE_21__["HomeComponent"],
        _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_34__["ApplicationgroupComponent"],
        _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_35__["UsersComponent"],
        _pages_root_applications_applications_component__WEBPACK_IMPORTED_MODULE_36__["ApplicationsComponent"],
        _pages_root_smtpaccounts_smtpaccounts_component__WEBPACK_IMPORTED_MODULE_37__["SmtpaccountsComponent"],
        _pages_root_oauthsettings_oauthsettings_component__WEBPACK_IMPORTED_MODULE_38__["OauthsettingsComponent"],
        _pages_root_eventlogs_eventlogs_component__WEBPACK_IMPORTED_MODULE_39__["EventlogsComponent"],
        _pages_root_resourceusage_resourceusage_component__WEBPACK_IMPORTED_MODULE_40__["ResourceusageComponent"],
        _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_41__["ErrorpageComponent"],
        _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_42__["BackmenulayoutComponent"],
        _pages_login_login_component__WEBPACK_IMPORTED_MODULE_43__["LoginComponent"],
        _pages_forgotpassword_forgotpassword_component__WEBPACK_IMPORTED_MODULE_44__["ForgotpasswordComponent"],
        _layout_fullmenulayout_fullmenulayout_component__WEBPACK_IMPORTED_MODULE_47__["FullmenulayoutComponent"],
        _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_48__["TablehelperComponent"],
        _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_49__["ApplicationdetailComponent"],
        _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_50__["UserdetailComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_1__["BrowserAnimationsModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_45__["HttpClientModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
        primeng_card__WEBPACK_IMPORTED_MODULE_7__["CardModule"],
        primeng_toolbar__WEBPACK_IMPORTED_MODULE_8__["ToolbarModule"],
        primeng_button__WEBPACK_IMPORTED_MODULE_10__["ButtonModule"],
        primeng_inputtext__WEBPACK_IMPORTED_MODULE_9__["InputTextModule"],
        primeng_splitbutton__WEBPACK_IMPORTED_MODULE_11__["SplitButtonModule"],
        primeng_menubar__WEBPACK_IMPORTED_MODULE_12__["MenubarModule"],
        primeng_panelmenu__WEBPACK_IMPORTED_MODULE_13__["PanelMenuModule"],
        primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_22__["BreadcrumbModule"],
        primeng_dropdown__WEBPACK_IMPORTED_MODULE_23__["DropdownModule"],
        primeng_chart__WEBPACK_IMPORTED_MODULE_29__["ChartModule"],
        primeng_blockui__WEBPACK_IMPORTED_MODULE_24__["BlockUIModule"],
        primeng_divider__WEBPACK_IMPORTED_MODULE_17__["DividerModule"],
        primeng_progressspinner__WEBPACK_IMPORTED_MODULE_33__["ProgressSpinnerModule"],
        primeng_messages__WEBPACK_IMPORTED_MODULE_31__["MessagesModule"],
        primeng_message__WEBPACK_IMPORTED_MODULE_32__["MessageModule"],
        primeng_dialog__WEBPACK_IMPORTED_MODULE_14__["DialogModule"],
        primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_16__["DynamicDialogModule"],
        primeng_panel__WEBPACK_IMPORTED_MODULE_15__["PanelModule"],
        primeng_table__WEBPACK_IMPORTED_MODULE_30__["TableModule"],
        primeng_selectbutton__WEBPACK_IMPORTED_MODULE_25__["SelectButtonModule"],
        primeng_steps__WEBPACK_IMPORTED_MODULE_26__["StepsModule"],
        primeng_inputnumber__WEBPACK_IMPORTED_MODULE_27__["InputNumberModule"],
        primeng_autocomplete__WEBPACK_IMPORTED_MODULE_28__["AutoCompleteModule"]] }); })();


/***/ }),

/***/ "brE3":
/*!*********************************************************************!*\
  !*** ./src/app/pages/root/oauthsettings/oauthsettings.component.ts ***!
  \*********************************************************************/
/*! exports provided: OauthsettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OauthsettingsComponent", function() { return OauthsettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class OauthsettingsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'LDAP Management' }
        ];
    }
}
OauthsettingsComponent.ɵfac = function OauthsettingsComponent_Factory(t) { return new (t || OauthsettingsComponent)(); };
OauthsettingsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: OauthsettingsComponent, selectors: [["app-oauthsettings"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function OauthsettingsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJvYXV0aHNldHRpbmdzLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "brkH":
/*!*****************************************************!*\
  !*** ./src/app/pages/root/users/users.component.ts ***!
  \*****************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/root/usermanager.service */ "4nGI");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/button */ "jIHw");












function UsersComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 6);
} }
function UsersComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function UsersComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deleteConfirmation($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.userlist)("header", ctx_r1.usrheader)("wsearch", true)("actionbtn", ctx_r1.usractionbtn)("colnames", ctx_r1.usrcolname)("colwidth", ctx_r1.usrcolwidth)("colclasshalign", ctx_r1.usrcolhalign)("addbtnlink", ctx_r1.usraddbtn)("colmark", 2);
} }
function UsersComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UsersComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UsersComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.deleteUser(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
class UsersComponent {
    constructor(authservice, dialogService, messageService, userService) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.userService = userService;
        this.display = false;
        this.selectedUser = {};
        this.isGroup = false;
        this.userlist = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.usrheader = [{ 'label': 'Name', 'sort': 'fullname' }, { 'label': 'User ID', 'sort': 'userid' }, { 'label': 'Status', 'sort': 'active' }, { 'label': 'Group App', 'sort': 'orgname' }, { 'label': 'Group Menu', 'sort': 'groupname' }];
        this.usrcolname = ["fullname", "userid", "active", "orgname", "groupname"];
        this.usrcolhalign = ["", "", "p-text-center", ""];
        this.usrcolwidth = ["", "", { 'width': '110px' }, ""];
        // orgcollinghref:any = {'url':'#','label':'Application'}
        this.usractionbtn = [1, 1, 1, 1, 0];
        this.usraddbtn = { 'route': 'detail', 'label': 'Add Data' };
    }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Users Management' }
        ];
        this.authservice.whoAmi().subscribe((value) => {
            // console.log(">>> User Info : "+JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
        });
        this.refreshingUser();
    }
    refreshingUser() {
        this.isFetching = true;
        this.authservice.whoAmi().subscribe((data) => {
            // console.log(">>>>>>> "+JSON.stringify(data));
            if ((data.status = 200)) {
                this.userService
                    .retriveUsers()
                    .subscribe((orgall) => {
                    console.log('>>>>>>> ' + JSON.stringify(orgall));
                    this.userlist = orgall.data;
                    if (this.userlist.length < 1) {
                        let objtmp = { "fullname": "No records", "userid": "No records", "active": "No records", "orgname": "No records" };
                        this.userlist = [];
                        this.userlist.push(objtmp);
                    }
                    this.isFetching = false;
                });
            }
        });
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedUser = data;
    }
    deleteUser() {
        console.log(this.selectedUser);
        let user = this.selectedUser;
        const payload = { user };
        this.userService
            .deleteUser(payload)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingUser();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: 'success',
            summary: 'Deleted',
            detail: message,
        });
    }
}
UsersComponent.ɵfac = function UsersComponent_Factory(t) { return new (t || UsersComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__["UsermanagerService"])); };
UsersComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: UsersComponent, selectors: [["app-users"]], decls: 8, vars: 11, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete User", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "colclasshalign", "addbtnlink", "colmark", "datadeleted"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function UsersComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, UsersComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, UsersComponent_div_3_Template, 2, 9, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function UsersComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Users?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, UsersComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.userlist.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.userlist.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_5__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_7__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_8__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_10__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_11__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2Vycy5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "bzlq":
/*!********************************************************!*\
  !*** ./src/app/pages/errorpage/errorpage.component.ts ***!
  \********************************************************/
/*! exports provided: ErrorpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorpageComponent", function() { return ErrorpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class ErrorpageComponent {
    constructor() { }
    ngOnInit() {
    }
}
ErrorpageComponent.ɵfac = function ErrorpageComponent_Factory(t) { return new (t || ErrorpageComponent)(); };
ErrorpageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ErrorpageComponent, selectors: [["app-errorpage"]], decls: 6, vars: 0, consts: [["src", "assets/logos/404.png", "height", "320"], [2, "font-size", "24px"]], template: function ErrorpageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "p", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Oops, something wrong!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, " please contact your administrator.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlcnJvcnBhZ2UuY29tcG9uZW50LnNjc3MifQ== */", "body[_ngcontent-%COMP%] {\n        background: #dee2e6\n    }\n\n    section[_ngcontent-%COMP%] {\n        position: absolute;\n        top: 40%;\n        left: 50%;\n        margin-right: -50%;\n        transform: translate(-50%, -50%);\n        text-align: center;\n    }"] });


/***/ }),

/***/ "cygB":
/*!*********************************************!*\
  !*** ./src/app/services/backend.service.ts ***!
  \*********************************************/
/*! exports provided: BackendService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackendService", function() { return BackendService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _env_env_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../env/env.service */ "usjD");

// import { environment } from '@environtment';




class BackendService {
    constructor(httpClient, environment) {
        this.httpClient = httpClient;
        this.environment = environment;
    }
    post(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        console.log(">>>> POST " + JSON.stringify(payload));
        return this.httpClient
            .post(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    get(path, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    put(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient.put(url, payload).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    patch(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient
            .patch(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    delete(path, payload, authorized) {
        const url = this.environment.apiUrl + path;
        return this.httpClient
            .delete(url, payload)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])(this.handleError));
    }
    handleError(error) {
        console.log('error occured ', error);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["throwError"])(error);
    }
}
BackendService.ɵfac = function BackendService_Factory(t) { return new (t || BackendService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_env_env_service__WEBPACK_IMPORTED_MODULE_4__["EnvService"])); };
BackendService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: BackendService, factory: BackendService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "fNSO":
/*!**************************************!*\
  !*** ./src/app/guard/guard.guard.ts ***!
  \**************************************/
/*! exports provided: GuardGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuardGuard", function() { return GuardGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "lGQG");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");




class GuardGuard {
    constructor(authService, router) {
        this.authService = authService;
        this.router = router;
    }
    canActivate(route, state) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.authService.isLoggedIn()) {
                console.log(' sudah logged in');
                return yield this.checkPage(this.authService, route, state, this.router);
            }
            else {
                this.router.navigate(['/auth/login']);
                return false;
            }
        });
    }
    checkPage(authService, routeCp, stateCp, router) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const promise = new Promise(function (resolve, reject) {
                // setTimeout(function() {
                authService.whoAmi().subscribe((value) => {
                    resolve('Promise returns after 1.5 second!');
                    console.log(JSON.stringify(value.data));
                    // let lvlTenant = parseInt(value.data.leveltenant);
                    // if (lvlTenant > 0) {
                    //   let anypage = false;
                    //   value.data.sidemenus.forEach((module: any) => {
                    //     module.items.forEach((menus: any) => {
                    //       if (menus.routerLink) {
                    //         let routerLink = menus.routerLink;
                    //         let mainModule = routerLink.split('/');
                    //         let module = mainModule[2];
                    //         let currentPath = stateCp.url;
                    //         if (
                    //           currentPath.includes(module) ||
                    //           currentPath.includes('profile')
                    //         )
                    //           anypage = true;
                    //       }
                    //     });
                    //   });
                    //   if (stateCp.url === '/home') anypage = true;
                    //   //if (stateCp.url === '/vam') anypage = true;
                    //   console.log('Ada Halaman ' + stateCp.url);
                    //   if (anypage) {
                    //     resolve('Promise returns after 1.5 second!');
                    //   } else {
                    //     router.navigate(['/noauth/err']);
                    //     reject(anypage);
                    //   }
                    // } else {
                    //   resolve('Promise returns after 1.5 second!');
                    // }
                });
            });
            return promise.then(function (value) {
                console.log(value);
                return true;
                // Promise returns after 1.5 second!
            });
        });
    }
}
GuardGuard.ɵfac = function GuardGuard_Factory(t) { return new (t || GuardGuard)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"])); };
GuardGuard.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: GuardGuard, factory: GuardGuard.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "hwT4":
/*!*******************************************************************!*\
  !*** ./src/app/pages/root/applications/applications.component.ts ***!
  \*******************************************************************/
/*! exports provided: ApplicationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationsComponent", function() { return ApplicationsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class ApplicationsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Applications Management' }
        ];
    }
}
ApplicationsComponent.ɵfac = function ApplicationsComponent_Factory(t) { return new (t || ApplicationsComponent)(); };
ApplicationsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ApplicationsComponent, selectors: [["app-applications"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function ApplicationsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBsaWNhdGlvbnMuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "j149":
/*!**************************************************************!*\
  !*** ./src/app/generic/tablehelper/tablehelper.component.ts ***!
  \**************************************************************/
/*! exports provided: TablehelperComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TablehelperComponent", function() { return TablehelperComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/table */ "rEr+");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");








const _c0 = ["dt"];
function TablehelperComponent_ng_template_2_button_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "button", 11);
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("label", ctx_r4.addbtnlink.label);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", ctx_r4.addbtnlink.route);
} }
function TablehelperComponent_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_ng_template_2_button_1_Template, 1, 2, "button", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "input", 9, 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function TablehelperComponent_ng_template_2_Template_input_input_4_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r7); const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5); _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](1); return _r0.filterGlobal(_r5.value, "contains"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.actionbtn[0] == 1);
} }
function TablehelperComponent_ng_template_3_th_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-sortIcon", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const head_r9 = ctx.$implicit;
    const i_r10 = ctx.index;
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](ctx_r8.colwidth[i_r10]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("pSortableColumn", head_r9.sort);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", head_r9.label, " ");
} }
const _c1 = function () { return { "width": "200px" }; };
function TablehelperComponent_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_ng_template_3_th_1_Template, 3, 4, "th", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "th", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Action");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r2.header);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c1));
} }
function TablehelperComponent_ng_template_4_td_1_ng_template_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0);
} if (rf & 2) {
    const data_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const record_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", record_r11[data_r16], " ");
} }
function TablehelperComponent_ng_template_4_td_1_ng_template_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r19.collinkaction.label);
} }
function TablehelperComponent_ng_template_4_td_1_ng_template_3_i_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 25);
} }
function TablehelperComponent_ng_template_4_td_1_ng_template_3_i_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "i", 26);
} }
function TablehelperComponent_ng_template_4_td_1_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, TablehelperComponent_ng_template_4_td_1_ng_template_3_i_0_Template, 1, 0, "i", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_ng_template_4_td_1_ng_template_3_i_1_Template, 1, 0, "i", 24);
} if (rf & 2) {
    const data_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const record_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", record_r11[data_r16] === 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", record_r11[data_r16] === 0);
} }
function TablehelperComponent_ng_template_4_td_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_ng_template_4_td_1_ng_template_1_Template, 1, 1, "ng-template", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_ng_template_4_td_1_ng_template_2_Template, 2, 1, "ng-template", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_ng_template_4_td_1_ng_template_3_Template, 2, 2, "ng-template", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const j_r17 = ctx.index;
    const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](ctx_r12.colclasshalign[j_r17]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r17 != ctx_r12.collink && j_r17 != ctx_r12.colmark);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r17 == ctx_r12.collink);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", j_r17 == ctx_r12.colmark);
} }
const _c2 = function (a0) { return [a0]; };
function TablehelperComponent_ng_template_4_button_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "button", 27);
} if (rf & 2) {
    const record_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c2, ctx_r13.addbtnlink.route + "/" + record_r11.id));
} }
function TablehelperComponent_ng_template_4_button_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "button", 28);
} if (rf & 2) {
    const record_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](1, _c2, ctx_r14.addbtnlink.route + "#/" + record_r11.id));
} }
function TablehelperComponent_ng_template_4_button_5_Template(rf, ctx) { if (rf & 1) {
    const _r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TablehelperComponent_ng_template_4_button_5_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r31); const record_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit; const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r29.deleteConfirmation(record_r11); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TablehelperComponent_ng_template_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TablehelperComponent_ng_template_4_td_1_Template, 4, 5, "td", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "td", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_ng_template_4_button_3_Template, 1, 3, "button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TablehelperComponent_ng_template_4_button_4_Template, 1, 3, "button", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TablehelperComponent_ng_template_4_button_5_Template, 1, 0, "button", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.colnames);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.actionbtn[4] == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.actionbtn[2] == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.actionbtn[3] == 1);
} }
const _c3 = function () { return [5, 10, 25, 50]; };
class TablehelperComponent {
    constructor() {
        this.first = 0;
        this.rows = 5;
        this.datadeleted = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngOnInit() {
        // console.log("Data : "+JSON.stringify(this.records));
        // console.log("Header "+JSON.stringify(this.header));
        // console.log("actbutton "+JSON.stringify(this.actionbtn));
        // console.log("addbtn "+JSON.stringify(this.addbtnlink));
    }
    deleteConfirmation(payload) {
        // console.log("Di Emit nih "+JSON.stringify(payload));
        this.datadeleted.emit(payload);
    }
    next() {
        this.first = this.first + this.rows;
    }
    prev() {
        this.first = this.first - this.rows;
    }
    reset() {
        this.first = 0;
    }
    isLastPage() {
        return this.records
            ? this.first === this.records.length - this.rows
            : true;
    }
    isFirstPage() {
        return this.records ? this.first === 0 : true;
    }
}
TablehelperComponent.ɵfac = function TablehelperComponent_Factory(t) { return new (t || TablehelperComponent)(); };
TablehelperComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TablehelperComponent, selectors: [["app-tablehelper"]], viewQuery: function TablehelperComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dt = _t.first);
    } }, inputs: { records: "records", wsearch: "wsearch", header: "header", colnames: "colnames", colwidth: "colwidth", colclasshalign: "colclasshalign", colmark: "colmark", collink: "collink", collinkaction: "collinkaction", actionbtn: "actionbtn", addbtnlink: "addbtnlink" }, outputs: { datadeleted: "datadeleted" }, decls: 5, vars: 7, consts: [["dataKey", "id", "styleClass", "p-datatable-customs p-datatable-gridlines", "currentPageReportTemplate", "Showing {first} to {last} of {totalRecords} entries", 3, "value", "rows", "showCurrentPageReport", "rowsPerPageOptions", "paginator", "globalFilterFields"], ["dt", ""], ["pTemplate", "caption"], ["pTemplate", "header"], ["pTemplate", "body"], [1, "p-d-flex"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus-circle", "class", "p-primary-btn", 3, "routerLink", "label", 4, "ngIf"], [1, "p-input-icon-right", "p-ml-auto"], [1, "pi", "pi-search"], ["pInputText", "", "type", "text", "placeholder", "Search keyword", 3, "input"], ["myInput", ""], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-plus-circle", 1, "p-primary-btn", 3, "routerLink", "label"], ["class", "p-text-center", 3, "pSortableColumn", "style", 4, "ngFor", "ngForOf"], [1, "p-text-center"], [1, "p-text-center", 3, "pSortableColumn"], ["field", "head.sort"], ["class", "vertAlignCenter ", 3, "class", 4, "ngFor", "ngForOf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-search", "class", "p-button-rounded p-button-text cstnoheader", 3, "routerLink", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-pencil", "class", "p-button-rounded p-button-text cstnoheader", 3, "routerLink", 4, "ngIf"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-times", "style", "color: red;", "class", "p-button-rounded p-button-secondary p-button-text cstnoheader", 3, "click", 4, "ngIf"], [1, "vertAlignCenter"], [3, "ngIf"], ["href", "#"], ["class", "pi pi-check-circle", "style", "color: green;", 4, "ngIf"], ["class", "pi pi-times-circle", "style", "color: red;", 4, "ngIf"], [1, "pi", "pi-check-circle", 2, "color", "green"], [1, "pi", "pi-times-circle", 2, "color", "red"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-search", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "routerLink"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-pencil", 1, "p-button-rounded", "p-button-text", "cstnoheader", 3, "routerLink"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-times", 1, "p-button-rounded", "p-button-secondary", "p-button-text", "cstnoheader", 2, "color", "red", 3, "click"]], template: function TablehelperComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-table", 0, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TablehelperComponent_ng_template_2_Template, 6, 1, "ng-template", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TablehelperComponent_ng_template_3_Template, 4, 4, "ng-template", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TablehelperComponent_ng_template_4_Template, 6, 4, "ng-template", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.records)("rows", 5)("showCurrentPageReport", true)("rowsPerPageOptions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c3))("paginator", true)("globalFilterFields", ctx.colnames);
    } }, directives: [primeng_table__WEBPACK_IMPORTED_MODULE_1__["Table"], primeng_api__WEBPACK_IMPORTED_MODULE_2__["PrimeTemplate"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgIf"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_4__["InputText"], primeng_button__WEBPACK_IMPORTED_MODULE_5__["ButtonDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLink"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], primeng_table__WEBPACK_IMPORTED_MODULE_1__["SortableColumn"], primeng_table__WEBPACK_IMPORTED_MODULE_1__["SortIcon"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0YWJsZWhlbHBlci5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "lGQG":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./backend.service */ "cygB");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





class AuthService {
    constructor(sessionStorage, backend, router) {
        this.sessionStorage = sessionStorage;
        this.backend = backend;
        this.router = router;
        this.response = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](null);
        this.authenticated = false;
        this.tokenStorage = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"]('');
        this.sharedMessage = this.tokenStorage.asObservable();
    }
    isLoggedIn() {
        console.log("Apakah sudah is auth ? " + this.authenticated);
        const token = sessionStorage.getItem('accesstoken');
        if (token !== null)
            this.authenticated = true;
        return this.authenticated;
    }
    loggedOut() {
        this.sessionStorage.clear();
        this.authenticated = false;
    }
    // whoAmi(){
    //   const url = 'adm/auth/who';
    //   return this.backend.get(url);
    // }
    whoAmi() {
        const url = 'adm/auth/who';
        return this.backend.get(url);
    }
    changeAppLication(payload) {
        const url = 'adm/auth/changeapp';
        return this.backend.post(url, { "appid": payload });
    }
    setAuthStatus(status) {
        this.authenticated = status;
    }
    setToken(tokenIn) {
        this.tokenStorage.next(tokenIn);
    }
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_2__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_3__["BackendService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"])); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "mPzt":
/*!*********************************************!*\
  !*** ./src/app/env/env.service.provider.ts ***!
  \*********************************************/
/*! exports provided: EnvServiceFactory, EnvServiceProvider */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvServiceFactory", function() { return EnvServiceFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvServiceProvider", function() { return EnvServiceProvider; });
/* harmony import */ var _env_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./env.service */ "usjD");
 // import environment service
const EnvServiceFactory = () => {
    const env = new _env_service__WEBPACK_IMPORTED_MODULE_0__["EnvService"](); // Create env Instance
    // Read environment variables from browser window
    const browserWindow = window || {};
    const browserWindowEnv = browserWindow['env'] || {};
    // Assign environment variables from browser window to env
    // In the current implementation, properties from env.js overwrite defaults from the EnvService.
    // If needed, a deep merge can be performed here to merge properties instead of overwriting them.
    for (const key in browserWindowEnv) {
        console.log(">>>>>>>>>>>>>> KEY S " + key);
        if (browserWindowEnv.hasOwnProperty(key)) {
            env[key] = window['env'][key];
        }
    }
    return env;
};
const EnvServiceProvider = {
    provide: _env_service__WEBPACK_IMPORTED_MODULE_0__["EnvService"],
    useFactory: EnvServiceFactory,
    deps: [],
};


/***/ }),

/***/ "mnfZ":
/*!**********************************************************!*\
  !*** ./src/app/interceptors/interceptor-http.service.ts ***!
  \**********************************************************/
/*! exports provided: InterceptorHttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterceptorHttpService", function() { return InterceptorHttpService; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _loader_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../loader/loader.service */ "t0Il");
/* harmony import */ var angular_web_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! angular-web-storage */ "A8Ym");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "lGQG");





class InterceptorHttpService {
    constructor(loaderService, session, authservice) {
        this.loaderService = loaderService;
        this.session = session;
        this.authservice = authservice;
    }
    intercept(req, next) {
        if (req.method != 'GET' && !req.headers.has('Content-Type')) {
            req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
        }
        req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
        // console.log(">>>>>>>>>>>>>>>>>>>>>>> MASUK INTERCEPT NIH  "+ req.method)
        this.loaderService.isLoading.next(true);
        req = this.updateAccessToken(req);
        // req = this.updateAT(req);
        return next.handle(req).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["finalize"])(() => {
            this.loaderService.isLoading.next(false);
        }));
    }
    updateAccessToken(req) {
        const authToken = this.session.get("accesstoken");
        if (authToken) {
            req = req.clone({
                headers: req.headers.set("Authorization", `Bearer ${authToken}`)
            });
        }
        return req;
    }
}
InterceptorHttpService.ɵfac = function InterceptorHttpService_Factory(t) { return new (t || InterceptorHttpService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_loader_loader_service__WEBPACK_IMPORTED_MODULE_2__["LoaderService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](angular_web_storage__WEBPACK_IMPORTED_MODULE_3__["SessionStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"])); };
InterceptorHttpService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: InterceptorHttpService, factory: InterceptorHttpService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "nMyi":
/*!*******************************************************************!*\
  !*** ./src/app/layout/backmenulayout/backmenulayout.component.ts ***!
  \*******************************************************************/
/*! exports provided: BackmenulayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BackmenulayoutComponent", function() { return BackmenulayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/toolbar */ "5EWq");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");




const _c0 = function () { return { "background": "none", "border": "none!important" }; };
class BackmenulayoutComponent {
    constructor() { }
    ngOnInit() {
    }
}
BackmenulayoutComponent.ɵfac = function BackmenulayoutComponent_Factory(t) { return new (t || BackmenulayoutComponent)(); };
BackmenulayoutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BackmenulayoutComponent, selectors: [["app-backmenulayout"]], decls: 15, vars: 3, consts: [["lang", "en"], ["charset", "UTF-8"], ["http-equiv", "X-UA-Compatible", "content", "IE=edge"], ["name", "viewport", "content", "width=device-width, initial-scale=1.0"], ["id", "wrapper"], ["id", "contentliquid"], ["id", "contentnomenu"], [1, "p-toolbar-group-left", 2, "padding-left", "10px", "padding-top", "10px", "padding-bottom", "10px"], ["pButton", "", "pRipple", "", "type", "button", "icon", "pi pi-arrow-left", 1, "p-button-rounded", "p-button-text", "p-button-plain"]], template: function BackmenulayoutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "html", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "head");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "meta", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "meta", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "title");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Error Page");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "body");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "p-toolbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](2, _c0));
    } }, directives: [primeng_toolbar__WEBPACK_IMPORTED_MODULE_1__["Toolbar"], primeng_button__WEBPACK_IMPORTED_MODULE_2__["ButtonDirective"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJiYWNrbWVudWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "q50L":
/*!*********************************************************************!*\
  !*** ./src/app/pages/root/resourceusage/resourceusage.component.ts ***!
  \*********************************************************************/
/*! exports provided: ResourceusageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResourceusageComponent", function() { return ResourceusageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class ResourceusageComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Resource Usages' }
        ];
    }
}
ResourceusageComponent.ɵfac = function ResourceusageComponent_Factory(t) { return new (t || ResourceusageComponent)(); };
ResourceusageComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ResourceusageComponent, selectors: [["app-resourceusage"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function ResourceusageComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNvdXJjZXVzYWdlLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "qUOm":
/*!*******************************************************************!*\
  !*** ./src/app/pages/root/smtpaccounts/smtpaccounts.component.ts ***!
  \*******************************************************************/
/*! exports provided: SmtpaccountsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SmtpaccountsComponent", function() { return SmtpaccountsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");


class SmtpaccountsComponent {
    constructor() { }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'SMTP Management' }
        ];
    }
}
SmtpaccountsComponent.ɵfac = function SmtpaccountsComponent_Factory(t) { return new (t || SmtpaccountsComponent)(); };
SmtpaccountsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: SmtpaccountsComponent, selectors: [["app-smtpaccounts"]], decls: 1, vars: 2, consts: [[3, "home", "model"]], template: function SmtpaccountsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_1__["Breadcrumb"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzbXRwYWNjb3VudHMuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "sBLG":
/*!***************************************************************************!*\
  !*** ./src/app/pages/root/applicationgroup/applicationgroup.component.ts ***!
  \***************************************************************************/
/*! exports provided: ApplicationgroupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplicationgroupComponent", function() { return ApplicationgroupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! primeng/dynamicdialog */ "J7/z");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/root/organization.service */ "u0Pv");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/messages */ "dts7");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/dialog */ "/RsI");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/progressspinner */ "vKg+");
/* harmony import */ var _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../generic/tablehelper/tablehelper.component */ "j149");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/button */ "jIHw");












function ApplicationgroupComponent_p_progressSpinner_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-progressSpinner", 6);
} }
function ApplicationgroupComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "app-tablehelper", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("datadeleted", function ApplicationgroupComponent_div_3_Template_app_tablehelper_datadeleted_1_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.deleteConfirmation($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("records", ctx_r1.organizations)("header", ctx_r1.orgheader)("wsearch", true)("actionbtn", ctx_r1.orgactionbtn)("colnames", ctx_r1.orgcolname)("colwidth", ctx_r1.orgcolwidth)("collink", 3)("collinkaction", ctx_r1.orgcollinghref)("colclasshalign", ctx_r1.orgcolhalign)("addbtnlink", ctx_r1.orgaddbtn);
} }
function ApplicationgroupComponent_ng_template_7_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p-button", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_7_Template_p_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.display = false; });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "p-button", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ApplicationgroupComponent_ng_template_7_Template_p_button_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r7.deleteOrganization(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c0 = function () { return { width: "20vw" }; };
class ApplicationgroupComponent {
    constructor(authservice, dialogService, messageService, organizationService) {
        this.authservice = authservice;
        this.dialogService = dialogService;
        this.messageService = messageService;
        this.organizationService = organizationService;
        this.display = false;
        this.selectedOrganization = [];
        this.isFetching = false;
        this.userInfo = {};
        this.tokenID = "";
        this.orgheader = [{ 'label': 'Code', 'sort': 'orgcode' }, { 'label': 'Group Name', 'sort': 'orgname' }, { 'label': 'Description', 'sort': 'orgdescription' }, { 'label': 'Linked', 'sort': 'orgdescription' }, { 'label': 'Created-by', 'sort': 'created_by' }];
        this.orgcolname = ["orgcode", "orgname", "orgdescription", "application", "created_by"];
        this.orgcolhalign = ["p-text-center", "", "", "p-text-center", "p-text-center"];
        this.orgcolwidth = [{ 'width': '110px' }, "", "", { 'width': '120px' }, ""];
        this.orgcollinghref = { 'url': '#', 'label': 'Application' };
        this.orgactionbtn = [1, 1, 1, 1, 0];
        this.orgaddbtn = { 'route': 'detail', 'label': 'Add Data' };
        this.organizations = [];
    }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Applications Group' }
        ];
        this.authservice.whoAmi().subscribe((value) => {
            // console.log(">>> User Info : "+JSON.stringify(value));
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
        });
        this.refreshingApp();
    }
    refreshingApp() {
        this.isFetching = true;
        this.authservice.whoAmi().subscribe((data) => {
            // console.log(">>>>>>> "+JSON.stringify(data));
            if ((data.status = 200)) {
                this.organizationService
                    .retriveOrgByTenant()
                    .subscribe((orgall) => {
                    // console.log('>>>>>>> ' + JSON.stringify(orgall));
                    this.organizations = orgall.data;
                    if (this.organizations.length < 1) {
                        let objtmp = { "orgcode": "No records", "orgname": "No records", "orgdescription": "No records", "application": "No records", "created_by": "No records" };
                        this.organizations = [];
                        this.organizations.push(objtmp);
                    }
                    this.isFetching = false;
                });
            }
        });
    }
    deleteConfirmation(data) {
        console.log("Di Emit nih dari child " + JSON.stringify(data));
        this.display = true;
        this.selectedOrganization = data;
    }
    deleteOrganization() {
        console.log(this.selectedOrganization);
        let organization = this.selectedOrganization;
        const payload = { organization };
        this.organizationService
            .deleteOrg(payload)
            .subscribe((resp) => {
            console.log(resp);
            if (resp.status === 200) {
                this.showTopSuccess(resp.data);
            }
            this.display = false;
            this.refreshingApp();
        });
    }
    showTopSuccess(message) {
        this.messageService.add({
            severity: 'success',
            summary: 'Deleted',
            detail: message,
        });
    }
}
ApplicationgroupComponent.ɵfac = function ApplicationgroupComponent_Factory(t) { return new (t || ApplicationgroupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_dynamicdialog__WEBPACK_IMPORTED_MODULE_2__["DialogService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_4__["OrganizationService"])); };
ApplicationgroupComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: ApplicationgroupComponent, selectors: [["app-applicationgroup"]], decls: 8, vars: 11, consts: [[3, "home", "model"], ["class", "p-text-center", 4, "ngIf"], ["style", "padding-left: 10px;padding-right: 10px;", 4, "ngIf"], ["header", "Delete Organization", 3, "visible", "baseZIndex", "draggable", "resizable", "visibleChange"], [2, "font-size", "18"], ["pTemplate", "footer"], [1, "p-text-center"], [2, "padding-left", "10px", "padding-right", "10px"], [3, "records", "header", "wsearch", "actionbtn", "colnames", "colwidth", "collink", "collinkaction", "colclasshalign", "addbtnlink", "datadeleted"], ["label", "No", "styleClass", "p-button-text", 3, "click"], ["label", "Yes", "styleClass", "p-button-text", 3, "click"]], template: function ApplicationgroupComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ApplicationgroupComponent_p_progressSpinner_1_Template, 1, 0, "p-progressSpinner", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "p-messages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ApplicationgroupComponent_div_3_Template, 2, 10, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p-dialog", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("visibleChange", function ApplicationgroupComponent_Template_p_dialog_visibleChange_4_listener($event) { return ctx.display = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Are you sure want to delete this Organization?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, ApplicationgroupComponent_ng_template_7_Template, 2, 0, "ng-template", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("home", ctx.home)("model", ctx.breadcrumbs);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.organizations.length == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.organizations.length > 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleMap"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](10, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("visible", ctx.display)("baseZIndex", 10000)("draggable", false)("resizable", false);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_5__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], primeng_messages__WEBPACK_IMPORTED_MODULE_7__["Messages"], primeng_dialog__WEBPACK_IMPORTED_MODULE_8__["Dialog"], primeng_api__WEBPACK_IMPORTED_MODULE_3__["PrimeTemplate"], primeng_progressspinner__WEBPACK_IMPORTED_MODULE_9__["ProgressSpinner"], _generic_tablehelper_tablehelper_component__WEBPACK_IMPORTED_MODULE_10__["TablehelperComponent"], primeng_button__WEBPACK_IMPORTED_MODULE_11__["Button"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHBsaWNhdGlvbmdyb3VwLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "t0Il":
/*!******************************************!*\
  !*** ./src/app/loader/loader.service.ts ***!
  \******************************************/
/*! exports provided: LoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoaderService", function() { return LoaderService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


class LoaderService {
    constructor() {
        this.isLoading = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](false);
    }
}
LoaderService.ɵfac = function LoaderService_Factory(t) { return new (t || LoaderService)(); };
LoaderService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: LoaderService, factory: LoaderService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "u0Pv":
/*!*******************************************************!*\
  !*** ./src/app/services/root/organization.service.ts ***!
  \*******************************************************/
/*! exports provided: OrganizationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrganizationService", function() { return OrganizationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _backend_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../backend.service */ "cygB");


class OrganizationService {
    constructor(service) {
        this.service = service;
    }
    retriveOrgByTenant() {
        const url = 'adm/organization/allbytenantid';
        return this.service.get(url);
    }
    insertOrg(payload) {
        const url = 'adm/organization/addorganization';
        return this.service.post(url, payload);
    }
    retriveOrgByTenantAndOrgId(orgId) {
        const url = `adm/organization/retriveByTenantAndOrgId/${orgId}`;
        return this.service.get(url);
    }
    editOrg(payload) {
        const url = 'adm/organization/editorganization';
        return this.service.post(url, payload);
    }
    deleteOrg(payload) {
        const url = 'adm/organization/deleteorganization';
        return this.service.post(url, payload);
    }
}
OrganizationService.ɵfac = function OrganizationService_Factory(t) { return new (t || OrganizationService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_backend_service__WEBPACK_IMPORTED_MODULE_1__["BackendService"])); };
OrganizationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: OrganizationService, factory: OrganizationService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "usjD":
/*!************************************!*\
  !*** ./src/app/env/env.service.ts ***!
  \************************************/
/*! exports provided: EnvService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EnvService", function() { return EnvService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class EnvService {
    constructor() {
        this.apiUrl = "http://localhost:3013/";
    }
}
EnvService.ɵfac = function EnvService_Factory(t) { return new (t || EnvService)(); };
EnvService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: EnvService, factory: EnvService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./guard/guard.guard */ "fNSO");
/* harmony import */ var _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layout/backmenulayout/backmenulayout.component */ "nMyi");
/* harmony import */ var _layout_fullmenulayout_fullmenulayout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./layout/fullmenulayout/fullmenulayout.component */ "MUFR");
/* harmony import */ var _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./layout/mainmenulayout/mainmenulayout.component */ "SuKp");
/* harmony import */ var _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./layout/nomenulayout/nomenulayout.component */ "WQ6N");
/* harmony import */ var _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pages/errorpage/errorpage.component */ "bzlq");
/* harmony import */ var _pages_home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./pages/home/home.component */ "1LmZ");
/* harmony import */ var _pages_homeadmin_homeadmin_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./pages/homeadmin/homeadmin.component */ "GK+Y");
/* harmony import */ var _pages_login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages/login/login.component */ "D8EZ");
/* harmony import */ var _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationdetail/applicationdetail.component */ "7YUa");
/* harmony import */ var _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./pages/root/applicationgroup/applicationgroup.component */ "sBLG");
/* harmony import */ var _pages_root_applications_applications_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./pages/root/applications/applications.component */ "hwT4");
/* harmony import */ var _pages_root_eventlogs_eventlogs_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./pages/root/eventlogs/eventlogs.component */ "J4FQ");
/* harmony import */ var _pages_root_oauthsettings_oauthsettings_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./pages/root/oauthsettings/oauthsettings.component */ "brE3");
/* harmony import */ var _pages_root_resourceusage_resourceusage_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./pages/root/resourceusage/resourceusage.component */ "q50L");
/* harmony import */ var _pages_root_smtpaccounts_smtpaccounts_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./pages/root/smtpaccounts/smtpaccounts.component */ "qUOm");
/* harmony import */ var _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./pages/root/users/userdetail/userdetail.component */ "z+Ab");
/* harmony import */ var _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pages/root/users/users.component */ "brkH");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/core */ "fXoL");





















const routes = [
    { path: '', redirectTo: 'mgm/root', pathMatch: 'full' },
    {
        path: 'mgm',
        data: { title: 'Administrator' },
        component: _layout_mainmenulayout_mainmenulayout_component__WEBPACK_IMPORTED_MODULE_4__["MainmenulayoutComponent"],
        children: [
            { path: 'root', canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]], component: _pages_homeadmin_homeadmin_component__WEBPACK_IMPORTED_MODULE_8__["HomeadminComponent"] },
            { path: 'root', canActivate: [_guard_guard_guard__WEBPACK_IMPORTED_MODULE_1__["GuardGuard"]], children: [
                    { path: 'appgroup', component: _pages_root_applicationgroup_applicationgroup_component__WEBPACK_IMPORTED_MODULE_11__["ApplicationgroupComponent"] },
                    { path: 'appgroup', children: [
                            { path: 'detail',
                                component: _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_10__["ApplicationdetailComponent"]
                            },
                            { path: 'detail#/:id',
                                component: _pages_root_applicationgroup_applicationdetail_applicationdetail_component__WEBPACK_IMPORTED_MODULE_10__["ApplicationdetailComponent"]
                            },
                        ] },
                    { path: 'users', component: _pages_root_users_users_component__WEBPACK_IMPORTED_MODULE_18__["UsersComponent"] },
                    { path: 'users', children: [
                            { path: 'detail',
                                component: _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_17__["UserdetailComponent"]
                            },
                            { path: 'detail#/:id',
                                component: _pages_root_users_userdetail_userdetail_component__WEBPACK_IMPORTED_MODULE_17__["UserdetailComponent"]
                            },
                        ] },
                    { path: 'apps', component: _pages_root_applications_applications_component__WEBPACK_IMPORTED_MODULE_12__["ApplicationsComponent"] },
                    { path: 'smtpacc', component: _pages_root_smtpaccounts_smtpaccounts_component__WEBPACK_IMPORTED_MODULE_16__["SmtpaccountsComponent"] },
                    { path: 'oauth', component: _pages_root_oauthsettings_oauthsettings_component__WEBPACK_IMPORTED_MODULE_14__["OauthsettingsComponent"] },
                    { path: 'eventlog', component: _pages_root_eventlogs_eventlogs_component__WEBPACK_IMPORTED_MODULE_13__["EventlogsComponent"] },
                    { path: 'resources', component: _pages_root_resourceusage_resourceusage_component__WEBPACK_IMPORTED_MODULE_15__["ResourceusageComponent"] },
                ] }
        ],
    },
    {
        path: 'mgm',
        data: { title: 'krakatoa' },
        component: _layout_fullmenulayout_fullmenulayout_component__WEBPACK_IMPORTED_MODULE_3__["FullmenulayoutComponent"],
        children: [
            { path: 'admin', component: _pages_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"] },
        ],
    },
    { path: 'nopage',
        component: _layout_backmenulayout_backmenulayout_component__WEBPACK_IMPORTED_MODULE_2__["BackmenulayoutComponent"],
        children: [
            { path: '404', component: _pages_errorpage_errorpage_component__WEBPACK_IMPORTED_MODULE_6__["ErrorpageComponent"] },
        ]
    },
    { path: 'auth',
        data: { title: 'Login' },
        component: _layout_nomenulayout_nomenulayout_component__WEBPACK_IMPORTED_MODULE_5__["NomenulayoutComponent"],
        children: [
            { path: 'login',
                component: _pages_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"], }
        ]
    },
];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_19__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_19__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_19__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "xlh6":
/*!**************************************!*\
  !*** ./src/app/_files/appadmin.json ***!
  \**************************************/
/*! exports provided: 0, 1, 2, 3, default */
/***/ (function(module) {

module.exports = JSON.parse("[{\"label\":\"Dashboard\",\"icon\":\"pi pi-fw pi-home\",\"routerLink\":\"/mgm/root\"},{\"label\":\"Users Management\",\"expanded\":true,\"items\":[{\"label\":\"Application Group\",\"icon\":\"pi pi-fw pi-microsoft\",\"routerLink\":\"/mgm/root/appgroup\"},{\"label\":\"Users\",\"icon\":\"pi pi-fw pi-users\",\"routerLink\":\"/mgm/root/users\"}]},{\"label\":\"General\",\"expanded\":true,\"items\":[{\"label\":\"Applications\",\"icon\":\"pi pi-fw pi-desktop\",\"routerLink\":\"/mgm/root/apps\"}]},{\"label\":\"Monitoring\",\"expanded\":true,\"items\":[{\"label\":\"Event Log\",\"icon\":\"pi pi-fw pi-check-square\",\"routerLink\":\"/mgm/root/eventlog\"},{\"label\":\"Resource usage\",\"icon\":\"pi pi-fw pi-chart-line\",\"routerLink\":\"/mgm/root/resources\"}]}]");

/***/ }),

/***/ "z+Ab":
/*!*********************************************************************!*\
  !*** ./src/app/pages/root/users/userdetail/userdetail.component.ts ***!
  \*********************************************************************/
/*! exports provided: UserdetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserdetailComponent", function() { return UserdetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/root/usermanager.service */ "4nGI");
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/auth.service */ "lGQG");
/* harmony import */ var src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/root/organization.service */ "u0Pv");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/api */ "7zfz");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/breadcrumb */ "URcr");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/card */ "QIUk");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/inputtext */ "7kUa");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/autocomplete */ "V5BG");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/button */ "jIHw");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/dropdown */ "arFO");
















function UserdetailComponent_form_2_div_10_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "User Name is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_2_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_2_div_10_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r1.f.username.errors.required);
} }
function UserdetailComponent_form_2_div_16_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Email is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_2_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_2_div_16_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r2.f.emailname.errors.required);
} }
function UserdetailComponent_form_2_div_22_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Fullname is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_2_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_2_div_22_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r3.f.fullname.errors.required);
} }
function UserdetailComponent_form_2_div_29_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Group Apps is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_2_div_29_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_2_div_29_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r4.f.orgMlt.errors.required);
} }
function UserdetailComponent_form_2_div_35_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Initial Secret is required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function UserdetailComponent_form_2_div_35_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, UserdetailComponent_form_2_div_35_span_1_Template, 2, 0, "span", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r5.f.temppass.errors.required);
} }
function UserdetailComponent_form_2_div_37_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Activate user :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](4, "p-dropdown", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("options", ctx_r6.stateOptions);
} }
function UserdetailComponent_form_2_div_38_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, "Activate user :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](3, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](4, "p-dropdown", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("options", ctx_r7.stateOptionsEdit);
} }
function UserdetailComponent_form_2_Template(rf, ctx) { if (rf & 1) {
    const _r14 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "form", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngSubmit", function UserdetailComponent_form_2_Template_form_ngSubmit_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r14); const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r13.onSubmit(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "p-card", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](7, "label", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](8, "User name * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](9, "input", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](10, UserdetailComponent_form_2_div_10_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](11, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](13, "label", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](14, "Email * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](15, "input", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](16, UserdetailComponent_form_2_div_16_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](17, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](19, "label", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](20, "Full name * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](21, "input", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](22, UserdetailComponent_form_2_div_22_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](23, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](24, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](25, "label", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](26, "Group Applications * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](27, "div", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](28, "p-autoComplete", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("completeMethod", function UserdetailComponent_form_2_Template_p_autoComplete_completeMethod_28_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵrestoreView"](_r14); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](); return ctx_r15.filterOrg($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](29, UserdetailComponent_form_2_div_29_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](30, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](31, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](32, "label", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](33, "Initial Secret * :");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](34, "input", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](35, UserdetailComponent_form_2_div_35_Template, 2, 1, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](36, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](37, UserdetailComponent_form_2_div_37_Template, 5, 1, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](38, UserdetailComponent_form_2_div_38_Template, 5, 1, "div", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](39, "div", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](40, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](41, "div", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](42, "button", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("formGroup", ctx_r0.groupForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.username.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.emailname.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.fullname.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("suggestions", ctx_r0.orgSuggest)("multiple", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.orgMlt.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.temppass.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", !ctx_r0.isEdit);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.isEdit);
} }
class UserdetailComponent {
    constructor(router, activatedRoute, formBuilder, umService, authservice, organizationService, filterService, location) {
        var _a;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.umService = umService;
        this.authservice = authservice;
        this.organizationService = organizationService;
        this.filterService = filterService;
        this.location = location;
        this.extraInfo = {};
        this.isEdit = false;
        this.userId = null;
        this.stateOptions = [];
        this.stateOptionsEdit = [];
        this.leveltenant = 0;
        this.userInfo = {};
        this.selectedApps = [];
        this.tokenID = "";
        this.submitted = false;
        this.orgsData = [];
        this.appInfoActive = {};
        this.orgSuggest = {};
        this.user = {};
        this.formatedOrg = [];
        this.extraInfo = (_a = this.router.getCurrentNavigation()) === null || _a === void 0 ? void 0 : _a.finalUrl.toString();
        let checkurl = this.extraInfo.indexOf("%23") !== -1 ? true : false;
        console.log(">>>>>>>>>>> " + this.extraInfo);
        console.log(checkurl);
        if (checkurl)
            this.isEdit = true;
    }
    ngOnInit() {
        this.home = { icon: 'pi pi-home', routerLink: '/' };
        this.breadcrumbs = [
            { label: 'Users Management', command: (event) => {
                    this.location.back();
                }, url: "" }, { label: this.isEdit ? 'Edit data' : 'Add data' }
        ];
        this.stateOptions = [
            { label: 'Direct activated', value: 1 },
            { label: 'Activision link', value: 0 },
        ];
        this.stateOptionsEdit = [
            { label: 'Active', value: 1 },
            { label: 'Deactive', value: 0 },
        ];
        this.authservice.whoAmi().subscribe((value) => {
            this.userInfo = value.data;
            this.tokenID = value.tokenId;
            this.leveltenant = this.userInfo.leveltenant;
            this.groupForm = this.formBuilder.group({
                username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                emailname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                fullname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                orgMlt: [[], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                temppass: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                isactive: [0],
            });
            this.organizationService
                .retriveOrgByTenant()
                .subscribe((orgall) => {
                this.orgsData = orgall.data;
                this.formatOrgData(this.orgsData);
            });
            if (this.isEdit) {
                if (this.activatedRoute.snapshot.paramMap.get('id')) {
                    this.userId = this.activatedRoute.snapshot.paramMap.get('id');
                    this.umService
                        .retriveUsersById(this.userId)
                        .subscribe((result) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        console.log("Hasil Edit " + JSON.stringify(result.data));
                        // if (this.isOrganization) {
                        this.user.username = result.data.userid;
                        this.user.fullname = result.data.fullname;
                        this.user.emailname = result.data.bioemailactive;
                        this.user.org = result.data.id;
                        this.user.password = result.data.pwd;
                        this.user.active = result.data.active;
                        this.groupForm.patchValue({
                            username: this.user.username,
                            fullname: this.user.fullname,
                            emailname: this.user.emailname,
                            //groupobj: this.user.group,
                            //orgobj: this.user.org,
                            orgMlt: result.data.org,
                            temppass: this.user.password,
                            isactive: this.user.active,
                        });
                        // console.log(this.user);
                    }));
                }
            }
        });
    }
    get f() {
        return this.groupForm.controls;
    }
    formatOrgData(data) {
        data.map((dt) => {
            let formated = {};
            formated.name = dt.orgname;
            formated.id = dt.id;
            this.formatedOrg.push(formated);
        });
    }
    filterOrg(event) {
        let filtered = [];
        let query = event.query;
        for (let i = 0; i < this.formatedOrg.length; i++) {
            let country = this.formatedOrg[i];
            if (country.name.toLowerCase().indexOf(query.toLowerCase()) > -1) {
                filtered.push(country);
            }
        }
        this.orgSuggest = filtered;
    }
    onSubmit() {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l;
        this.submitted = true;
        console.log('>>>>>>>>IS NULL >>>> ' + this.leveltenant);
        // console.log('>>>>>>>> Payload ' + JSON.stringify(payload));
        if (this.groupForm.valid) {
            // event?.preventDefault;
            var groupacl = (_a = this.groupForm.get('orgobj')) === null || _a === void 0 ? void 0 : _a.value;
            let payload = {};
            if (!this.isEdit) {
                payload = {
                    fullname: (_b = this.groupForm.get('fullname')) === null || _b === void 0 ? void 0 : _b.value,
                    userid: (_c = this.groupForm.get('username')) === null || _c === void 0 ? void 0 : _c.value,
                    password: (_d = this.groupForm.get('temppass')) === null || _d === void 0 ? void 0 : _d.value,
                    org: (_e = this.groupForm.get('orgMlt')) === null || _e === void 0 ? void 0 : _e.value,
                    isactive: (_f = this.groupForm.get('isactive')) === null || _f === void 0 ? void 0 : _f.value,
                    emailname: (_g = this.groupForm.get('emailname')) === null || _g === void 0 ? void 0 : _g.value,
                };
                this.umService
                    .insertBySuper(payload)
                    .subscribe((result) => {
                    if (result.status === 200) {
                        this.location.back();
                    }
                });
            }
            else {
                payload = {
                    fullname: (_h = this.groupForm.get('fullname')) === null || _h === void 0 ? void 0 : _h.value,
                    userid: this.userId,
                    org: (_j = this.groupForm.get('orgMlt')) === null || _j === void 0 ? void 0 : _j.value,
                    isactive: (_k = this.groupForm.get('isactive')) === null || _k === void 0 ? void 0 : _k.value,
                    emailname: (_l = this.groupForm.get('emailname')) === null || _l === void 0 ? void 0 : _l.value,
                };
                this.umService
                    .updatebySuper(payload)
                    .subscribe((result) => {
                    // console.log(">>>>>>>> return "+JSON.stringify(result));
                    if (result.status === 200) {
                        this.location.back();
                    }
                });
            }
        }
        console.log(this.groupForm.valid);
    }
}
UserdetailComponent.ɵfac = function UserdetailComponent_Factory(t) { return new (t || UserdetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_root_usermanager_service__WEBPACK_IMPORTED_MODULE_4__["UsermanagerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](src_app_services_root_organization_service__WEBPACK_IMPORTED_MODULE_6__["OrganizationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](primeng_api__WEBPACK_IMPORTED_MODULE_7__["FilterService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_common__WEBPACK_IMPORTED_MODULE_8__["Location"])); };
UserdetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: UserdetailComponent, selectors: [["app-userdetail"]], decls: 3, vars: 3, consts: [[3, "model", "home"], [1, "wrapperinside"], ["style", "padding: 2px;", 3, "formGroup", "ngSubmit", 4, "ngIf"], [2, "padding", "2px", 3, "formGroup", "ngSubmit"], [2, "height", "20px"], [1, "p-my-2"], [1, "p-fluid"], [1, "p-grid"], [1, "p-col"], [1, "box"], ["for", "username", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "username", "formControlName", "username", "id", "username", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "p-field", 4, "ngIf"], [2, "height", "15px"], ["for", "emailname", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "emailname", "formControlName", "emailname", "id", "emailname", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "fullname", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "fullname", "formControlName", "fullname", "id", "fullname", "type", "text", "pInputText", "", "required", "", 1, "p-mt-2"], ["for", "orgMlt", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-10"], [2, "height", "8px"], ["formControlName", "orgMlt", "field", "name", 1, "autosrc", 3, "suggestions", "multiple", "completeMethod"], ["for", "temppass", 1, "p-col-12", "p-mb-2", "p-md-2", "p-mb-md-0"], ["name", "temppass", "formControlName", "temppass", "id", "temppass", "type", "password", "pInputText", "", "required", "", 1, "p-mt-2"], ["class", "box", 4, "ngIf"], [1, "p-field", "p-col", "p-mt-5"], [1, "p-formgrid", "p-grid", 2, "text-align", "right"], [1, "p-field", "p-col"], ["pButton", "", "pRipple", "", "type", "submit", "label", "Save", "icon", "pi pi-angle-right", "iconPos", "right", 1, "p-button-success"], [1, "p-field"], ["style", "color: red;", 4, "ngIf"], [2, "color", "red"], ["id", "isactive", "formControlName", "isactive", "optionLabel", "label", "optionValue", "value", 3, "options"]], template: function UserdetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "p-breadcrumb", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, UserdetailComponent_form_2_Template, 43, 10, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("model", ctx.breadcrumbs)("home", ctx.home);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.groupForm);
    } }, directives: [primeng_breadcrumb__WEBPACK_IMPORTED_MODULE_9__["Breadcrumb"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_ba"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], primeng_card__WEBPACK_IMPORTED_MODULE_10__["Card"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], primeng_inputtext__WEBPACK_IMPORTED_MODULE_11__["InputText"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], primeng_autocomplete__WEBPACK_IMPORTED_MODULE_12__["AutoComplete"], primeng_button__WEBPACK_IMPORTED_MODULE_13__["ButtonDirective"], primeng_dropdown__WEBPACK_IMPORTED_MODULE_14__["Dropdown"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2VyZGV0YWlsLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map