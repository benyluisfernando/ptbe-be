const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);

router.get("/", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;
    // console.log(">>>> GET TENANTS >>>> " + dcodeInfo.leveltenant);

    //       select jdata.iduser, jdata.category-> 'codeparam' AS param  FROM (SELECT iduser, json_build_object('codeparam', json_agg(ca.cd_busparam)) category
    // FROM public.cdid_adminnotification ca group by iduser) jdata;

    // select distinct ca.iduser, ctu.fullname, ctu.userid, ctu.active,ctug.idgroupuseracl,
    // CASE WHEN ctug.idgroupuseracl > 0 THEN  mug.groupname ELSE 'Administrator' END AS groupname, catg.param::TEXT param
    // from public.cdid_adminnotification ca
    // INNER JOIN cdid_tenant_user ctu ON ca.iduser = ctu.id
    // LEFT JOIN cdid_tenant_user_groupacl ctug ON ca.iduser = ctug.iduser
    // LEFT JOIN public.mst_usergroup mug ON ctug.idgroupuseracl = mug.id
    // INNER JOIN (select jdata.iduser, jdata.category-> 'codeparam' AS param  FROM (SELECT iduser, json_build_object('codeparam', json_agg(ca.cd_busparam))
    // category
    // FROM public.cdid_adminnotification ca where ca.idtenant=$1 group by iduser) jdata) catg ON ca.iduser = catg.iduser

    // SELECT cdcat.iduser, string_agg(cdcat.cd_busparam::text, ', ') FROM (select iduser, cd_busparam FROM public.cdid_adminnotification where idtenant=1
    //   group by iduser,cd_busparam) cdcat group by cdcat.iduser

    // select distinct ca.iduser, ctu.fullname, ctu.userid, ctu.active,ctug.idgroupuseracl, CASE WHEN ctug.idgroupuseracl > 0
    // THEN  mug.groupname ELSE 'Administrator' END AS groupname, category.cd_busparam from public.cdid_adminnotification ca
    // INNER JOIN cdid_tenant_user ctu ON ca.iduser = ctu.id
    // LEFT JOIN cdid_tenant_user_groupacl ctug ON ca.iduser = ctug.iduser
    // LEFT JOIN public.mst_usergroup mug ON ctug.idgroupuseracl = mug.id
    // INNER JOIN (SELECT cdcat.iduser, string_agg(cdcat.cd_busparam::text, ', ') cd_busparam
    // 			FROM (select iduser, cd_busparam FROM public.cdid_adminnotification where idtenant=1 group by iduser,cd_busparam) cdcat
    // 			group by cdcat.iduser) category ON ca.iduser = category.iduser

    /*
    var query =
      "select distinct ca.iduser as id, ctu.fullname, ctu.userid, ctu.active,ctug.idgroupuseracl, CASE WHEN ctug.idgroupuseracl > 0 THEN  mug.groupname ELSE 'Administrator' END AS groupname, category.cd_busparam, to_char(ca.created_at, 'YYYY-MM-DD HH:mm:ss') created_at from public.cdid_adminnotification ca INNER JOIN cdid_tenant_user ctu ON ca.iduser = ctu.id LEFT JOIN cdid_tenant_user_groupacl ctug ON ca.iduser = ctug.iduser LEFT JOIN public.mst_usergroup mug ON ctug.idgroupuseracl = mug.id INNER JOIN (SELECT cdcat.iduser, string_agg(cdcat.cd_busparam::text, ', ') cd_busparam FROM (select iduser, cd_busparam FROM public.cdid_adminnotification where idtenant=$1 group by iduser,cd_busparam) cdcat group by cdcat.iduser) category ON ca.iduser = category.iduser";
    const resp = await pool.query(query, [dcodeInfo.idtenant]);
    */
    // const resp = await pool.raw(
    //   "select distinct ca.iduser as id, ctu.fullname, ctu.userid, ctu.active,ctug.idgroupuseracl, CASE WHEN ctug.idgroupuseracl > 0 THEN  mug.groupname ELSE 'Administrator' END AS groupname, category.cd_busparam, to_char(ca.created_at, 'YYYY-MM-DD HH:mm:ss') created_at from cdid_adminnotification ca INNER JOIN cdid_tenant_user ctu ON ca.iduser = ctu.id LEFT JOIN cdid_tenant_user_groupacl ctug ON ca.iduser = ctug.iduser LEFT JOIN mst_usergroup mug ON ctug.idgroupuseracl = mug.id INNER JOIN (SELECT cdcat.iduser, string_agg(cdcat.cd_busparam::text, ', ') cd_busparam FROM (select iduser, cd_busparam FROM cdid_adminnotification where idtenant=? group by iduser,cd_busparam) cdcat group by cdcat.iduser) category ON ca.iduser = category.iduser",
    //   [dcodeInfo.idtenant]
    // );
    var jsonbody = [];
    const resp = await pool.select("*").from("cdid_adminnotification");
    // console.log(resp)
    if(resp.length > 0) {
      jsonbody= resp;
      res.status(200).json({ status: 200, data: jsonbody });
    } else {
      res.status(200).json({ status: 200, data: jsonbody });
    }
    
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;
    let userid = req.params.id;
    /*
    var query =
      "select  ca.iduser as id, ctu.fullname, ctu.userid, ctu.active, ctug.idgroupuseracl, CASE WHEN ctug.idgroupuseracl > 0 THEN  mug.groupname ELSE 'Administrator' END AS groupname,ca.cd_busparam, to_char(ca.created_at, 'YYYY-MM-DD HH:mm:ss') created_at from public.cdid_adminnotification ca INNER JOIN cdid_tenant_user ctu ON ca.iduser = ctu.id  LEFT JOIN cdid_tenant_user_groupacl ctug ON ca.iduser = ctug.iduser  LEFT JOIN public.mst_usergroup mug ON ctug.idgroupuseracl = mug.id where ca.idtenant =$1 and ca.iduser = $2";
    const resp = await pool.query(query, [dcodeInfo.idtenant, userid]);
    */
    // const resp = await pool.raw(
    //   "select  ca.iduser as id, ctu.fullname, ctu.userid, ctu.active, ctug.idgroupuseracl, CASE WHEN ctug.idgroupuseracl > 0 THEN  mug.groupname ELSE 'Administrator' END AS groupname,ca.cd_busparam, to_char(ca.created_at, 'YYYY-MM-DD HH:mm:ss') created_at from cdid_adminnotification ca INNER JOIN cdid_tenant_user ctu ON ca.iduser = ctu.id  LEFT JOIN cdid_tenant_user_groupacl ctug ON ca.iduser = ctug.iduser  LEFT JOIN mst_usergroup mug ON ctug.idgroupuseracl = mug.id where ca.idtenant =? and ca.iduser =?",
    //   [dcodeInfo.idtenant, userid]
    // );
    console.log("######################### ANJAAAAAAAAAAAAY ");
    // const resp = await pool
    //   .select("*")
    //   .from("cdid_adminnotification")
    //   .where({ iduser: id });
    

      const resp = await pool
      .select("*")
      .from("cdid_adminnotification")
      .where({ id: userid });


    /*
    const resp = await pool
    .select(
        "ca.iduser as id",
        "ctu.fullname",
        "ctu.userid",
        "ctu.active",
        "ctug.idgroupuseracl"
    )
    */


    var jsonbody = resp;
    console.log(jsonbody);
    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.post("/addData", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;

    // fullname: this.fullname,
    //       emailname: this.emailname,
    //       group:this.group

    
    const { iduser, type, fullname, emailname, group } = req.body;
    var datasToInserts = [];
    await type.map((tp) => {
      let dataToInsert = {
        iduser: iduser,
        cd_busparam: tp.label,
        idtenant: dcodeInfo.idtenant,
        idapp: dcodeInfo.apps[0].idapp,
        created_byid: dcodeInfo.id,
        fullname: fullname,
        email_user: emailname,
        devisi:group
      };
      datasToInserts.push(dataToInsert);
      // dataToInsert.push(iduser);
      // dataToInsert.push(tp.label);
      // dataToInsert.push(dcodeInfo.idtenant);
      // dataToInsert.push(dcodeInfo.apps[0].appid);
      // dataToInsert.push(dcodeInfo.id);
      // datasToInsert.push(dataToInsert);
    });

    // let formatedQuery = format(
    //   "INSERT INTO cdid_adminnotification(iduser, cd_busparam, idtenant, idapp, created_byid)  VALUES %L returning *",
    //   datasToInsert
    // );
    // console.log(formatedQuery);
    const resp = await pool("cdid_adminnotification").insert(datasToInserts);

    var jsonbody = resp;
    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.post("/deleteData", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;

    const { id, iduser, type } = req.body;
    var datasToInsert = [];
    /*
    let formatedQuery =
      "Delete  from public.cdid_adminnotification where iduser = $1 ";
    const resp = await pool.query(formatedQuery, [iduser]);
    */
    // const resp = await pool.raw(
    //   "DELETE FROM cdid_adminnotification WHERE iduser = ?",
    //   [iduser]
    // );
    const resp = await pool("cdid_adminnotification")
      .where({ id: id })
      .del();

    var jsonbody = resp;
    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.post("/editData", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    var tipeModule = 1;
    console.log(">>>>>>>>>>>>>>>>>>>>>> EDIT KAMPRET");

    console.log(dcodeInfo);

    const { id, iduser, type, fullname, emailname, group } = req.body;

    // let formatedQuery = "Delete from  cdid_adminnotification where iduser =$1 ";
    // let resp = await pool.query(formatedQuery, [iduser]);


    const resp = await pool("cdid_adminnotification")
      .where({ id: id })
      .del();

    var datasToInsert = [];
    await type.map((tp) => {
      let dataToInsert = {
        iduser: iduser,
        cd_busparam: tp.label,
        idtenant: dcodeInfo.idtenant,
        idapp: dcodeInfo.apps[0].idapp,
        created_byid: dcodeInfo.id,
        fullname: fullname,
        email_user: emailname,
        devisi:group
      };
      datasToInsert.push(dataToInsert);
    });

    // // let insertQuery = format(
    // //   "INSERT INTO cdid_adminnotification(iduser, cd_busparam, idtenant, idapp, created_byid)  VALUES %L returning *",
    // //   datasToInsert
    // // );

    resp = await pool("cdid_adminnotification").insert(datasToInsert);
    var jsonbody = resp;
    
    // var jsonbody = "Success"
    
    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

module.exports = router;
