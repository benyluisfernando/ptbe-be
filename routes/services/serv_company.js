const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const axios = require("axios");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const { insertGroup } = require("./utils/groupUtils");
router.use(checkAuth);
/*POST END*/

router.put("/updatestat/:id", async (req, res, next) => {
  try {
    const {
      id,
      modulename,
      created_byid,
      status,
      idapplication,
      modulecode,
      applabel,
    } = req.body;

    const resp = await pool("mst_moduleby_tenant")
      .where({ id: req.params.id })
      .update({ status: status });
    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
    // }
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "No User found" });
  }
});


router.post("/getCompany", async (req, res, next) => {
    const authHeader = req.headers.authorization;
  
    try {
      console.log();
      axios.get( "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:getCompany/getCompany",
        
        {
          auth: { username: "Administrator",
            password: "manage" },
        }
        )
        .then(async (resp) => {
          // console.log(JSON.stringify(resp.data));
        //   var userObj = resp.data.data;
          console.log(resp);
            // if (respUpdate.length > 0) {
              res.status(200).json({ status: 200, data: resp.data });
            // } 
            // else {
            //   res.status(500).json({
            //     status: 500,
            //     data: "Error insert m_systemparam ",
            //   });
            // }
          // return;
        })
        .catch((err) => {
          console.log(err);
          return;
        }); 
  
   
    } catch (err) {
      //console.log(err);
      next(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
});

router.get("/getCompany", async (req, res, next) => {
    const authHeader = req.headers.authorization;
  
    try {
      console.log();
      axios.get( "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:getCompany/getCompany",
        
        {
          auth: { username: "Administrator",
            password: "manage" },
        }
        )
        .then(async (resp) => {
          // console.log(JSON.stringify(resp.data));
        //   var userObj = resp.data.data;
          console.log(resp);
            // if (respUpdate.length > 0) {
              res.status(200).json({ status: 200, data: resp.data });
            // } 
            // else {
            //   res.status(500).json({
            //     status: 500,
            //     data: "Error insert m_systemparam ",
            //   });
            // }
          // return;
        })
        .catch((err) => {
          console.log(err);
          return;
        }); 
  
    } catch (err) {
      //console.log(err);
      next(err);
      res.status(500).json({ status: 500, data: "internal error" });
    }
});


router.post("/addCompany", async (req, res, next) => {
    const authHeader = req.headers.authorization;
    console.log("services channel add new2==> "+req.body.ACTION);
     
    try {
      if(req.body.ACTION=="INSERT"){
            let data = {
            COMPANY_CODE      :req.body.COMPANY_CODE,
            COMPANY_NAME      :req.body.COMPANY_NAME,
            COMPANY_SHORTNAME :req.body.COMPANY_SHORTNAME,
            HOST_CODE         :req.body.HOST_CODE,
            HOST_URL_INQUIRY  :req.body.HOST_URL_INQUIRY,
            HOST_URL_PAYMENT  :req.body.HOST_URL_PAYMENT,
            HOST_URL_REVERSAL :req.body.HOST_URL_REVERSAL,
            ACTION            :req.body.ACTION,
            ENABLED_FLAG      :req.body.ENABLED_FLAG,
            CREATE_WHO        :req.body.CREATE_WHO,
            CHANGE_WHO        :req.body.CHANGE_WHO, 
            };
             //console.log("services channel add new2==> "+JSON.stringify(req.body.CHANNEL_NAME));
            axios.post( "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:InsertCompanyTemp/insertCompanyTemp",
              
              {
                  data           
              })
              .then(async (resp) => {
          
                console.log(resp);
                    res.status(200).json({ status: 200, data: resp.data });
               
              })
              .catch((err) => {
                console.log(err);
                return;
              });
      }else if(req.body.ACTION=="DELETE"){
            let data = {
            COMPANY_CODE      :req.body.COMPANY_CODE,
            COMPANY_NAME      :req.body.COMPANY_NAME,
            COMPANY_SHORTNAME :req.body.COMPANY_SHORTNAME,
            HOST_CODE         :req.body.HOST_CODE,
            HOST_URL_INQUIRY  :req.body.HOST_URL_INQUIRY,
            HOST_URL_PAYMENT  :req.body.HOST_URL_PAYMENT,
            HOST_URL_REVERSAL :req.body.HOST_URL_REVERSAL,
            ACTION            :"DELETE",
            ENABLED_FLAG      :req.body.ENABLED_FLAG,
            CREATE_WHO        :req.body.CREATE_WHO,
            CHANGE_WHO        :req.body.CHANGE_WHO, 
            };
             //console.log("services channel add new2==> "+JSON.stringify(req.body.CHANNEL_NAME));
            axios.post( "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:InsertCompanyTemp/insertCompanyTemp",
               
              {
                  data           
              })
              .then(async (resp) => {
          
                console.log(resp);
                    res.status(200).json({ status: 200, data: resp.data });
               
              })
              .catch((err) => {
                console.log(err);
                return;
              });
      }else if(req.body.ACTION=="UPDATE"){
            let data = {
            COMPANY_CODE      :req.body.COMPANY_CODE,
            COMPANY_NAME      :req.body.COMPANY_NAME,
            COMPANY_SHORTNAME :req.body.COMPANY_SHORTNAME,
            HOST_CODE         :req.body.HOST_CODE,
            HOST_URL_INQUIRY  :req.body.HOST_URL_INQUIRY,
            HOST_URL_PAYMENT  :req.body.HOST_URL_PAYMENT,
            HOST_URL_REVERSAL :req.body.HOST_URL_REVERSAL,
            ACTION            :"UPDATE",
            ENABLED_FLAG      :req.body.ENABLED_FLAG,
            CREATE_WHO        :req.body.CREATE_WHO,
            CHANGE_WHO        :req.body.CHANGE_WHO, 
            };
             //console.log("services channel add new2==> "+JSON.stringify(req.body.CHANNEL_NAME));
            axios.post( "http://182.169.41.151:8886/restv2/billpayment.services.portal.ws:InsertCompanyTemp/insertCompanyTemp",
              
              {
                  data           
              })
              .then(async (resp) => {
          
                console.log(resp);
                    res.status(200).json({ status: 200, data: resp.data });
               
              })
              .catch((err) => {
                console.log(err);
                return;
              });
     
      }
  
    } catch (err) { 
      next(err);
      res.status(500).json({ status: 500, data: "internal error"+err });
    }
});



router.get("/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var tipeModule = 1;
    console.log(">>>>>>>>>>>> " + dcodeInfo.idtenant);
    console.log(">>>>>>>>>>>> " + dcodeInfo.leveltenant);

    let resp;

    if (dcodeInfo.leveltenant !== "0") {
      resp = await pool
        .select("mstmod.id", "mmod.*")
        .from("mst_module as mstmod ")

        .innerJoin(
          pool
            .select(
              "mmod.id",
              "mmod.modulename",
              "mmod.created_byid",
              "mmod.status",
              "mmod.idapplication",
              "mmod.modulecode",
              "mapp.applabel"
            )
            .from(
              pool
                .select("*")
                .from("mst_moduleby_tenant")
                .where({ idtenant: dcodeInfo.idtenant })
                .as("mmod")
            )
            .innerJoin(
              "mst_application as mapp",
              " mmod.idapplication",
              " mapp.id"
            )
            .as("mmod"),
          " mstmod.modulename",
          "mmod.modulename"
        )
        .where({ "mmod.idapplication": req.params.id })
        .orderBy("mmod.id");
    } else {
      resp = await pool
        .select(
          "mmod.id",
          "mmod.modulename",
          "mmod.created_byid",
          "mmod.status",
          "mmod.idapplication",
          "mmod.modulecode",
          "mapp.applabel"
        )
        .from("mst_moduleby_tenant as mmod")
        .innerJoin("mst_application as mapp", "mmod.idapplication", "mapp.id")
        .where({
          "mmod.idapplication": dcodeInfo.idtenant,
          "mmod.idtenant": req.params.id,
        })
        .orderBy("mmod.id");
    }

    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/menusmodule/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    // const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    console.log(req.params.id);

    // TokenArray = authHeader.split(" ");
    // dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
    let module = {};
    let menus = [];

    const resp = await pool
      .select("id", "modulename")
      .from("mst_module")
      .where({ id: req.params.id });

    module = resp[0];
    console.log(">>>>>>>>>>>>>>>>>>>>>>>>> MASUK SINI NIHHHHHHH");
    await pool
      .select("mmen.idmodule", "mmen.id", "mmen.title", "mmod.modulename")
      .from("mst_moduleby_tenant as mmod")
      .innerJoin("mst_menu as mmen", "mmod.id", "mmen.idmodule")
      .where({ "mmod.id": req.params.id })
      .then(async (result) => {
        menus = result;
        module["menus"] = menus;
        console.log(module);

        res.status(200).json({ status: 200, data: module });
      })
      .catch((err) => {
        console.log(err);
        next(err);
        res.status(500).json({ status: 500, data: "Error insert log Catch" });
      });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/modulesgroup/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = null;
    if (authHeader) {
      TokenArray = authHeader.split(" ");
      dcodeInfo = await jwttools.decryptdata(TokenArray[1]);

      const resp = await pool
        .select(
          "mtg.groupname",
          "mtg.idtenant",
          "mtg.created_byid",
          "mtg.created_at",
          "mtg.updated_at",
          "mtg.id",
          "mtg.issubgroup",
          "mtg.idapplication",
          "mtg.idowner",
          "mapp.appname",
          "mapp.applabel"
        )
        .from("mst_tenantgroup as mtg")
        .innerJoin("mst_application as mapp", "mapp.id", "mtg.idapplication")
        .where({
          "tg.idapplication": req.params.id,
          "mtg.idtenant": dcodeInfo.idtenant,
        });

      var jsonbody = resp;
      res.status(200).json({ status: 200, data: jsonbody });
    } else {
      res.status(500).json({ status: 500, data: "Not authorized" });
    }

    res.status(200).json({ status: 200, data: dcodeInfo });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

router.get("/menusmodule/:id/:groupid", async (req, res, next) => {
  try {
    var dcodeInfo = req.userData;
    let module = {};
    let menus = [];

    const resp = await pool
      .select("id", "modulename")
      .from("mst_moduleby_tenant")
      .where({ id: req.params.id });
    module = resp[0];

    let menusDataWithAcl = await pool
      .select(
        "mmen.idmodule",
        "mmen.id",
        "uga.fread",
        "uga.fupdate",
        "uga.fdelete",
        "uga.fcreate",
        "uga.fview",
        "uga.fapproval",
        "mmen.title",
        "mmod.modulename"
      )
      .from("mst_moduleby_tenant as mmod")
      .innerJoin("mst_menu as  mmen", "mmod.id", "mmen.idmodule")
      .rightJoin("mst_usergroupacl as uga", "mmen.id", "uga.idmenu")
      .where({ "mmod.id": req.params.id, "uga.idgroup": req.params.groupid });

    let menusById = await pool
      .select("*")
      .from("mst_menu")
      .where({ idmodule: req.params.id });

    let mergedMenus = [];
    await menusById.map(async (mid) => {
      let mergedMenu = {
        fcreate: 0,
        fdelete: 0,
        fread: 0,
        fupdate: 0,
        fview: 0,
        id: "",
        idmodule: "",
        modulename: "",
        title: "",
      };

      mergedMenu.id = mid.id;
      mergedMenu.modulename = mid.name;
      mergedMenu.title = mid.title;
      mergedMenus.push(mergedMenu);
    });

    menus = await mergedMenus.map((mergMen) => ({
      ...mergMen,
      ...menusDataWithAcl.find((acmMen) => acmMen.id === mergMen.id),
    }));
    module.menus = menus;
    let jsonbody = module;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});

module.exports = router;

