const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");

const checkAuth = require("../../middleware/check-auth");
router.use(checkAuth);

router.get("/allbytenantid", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const dcodeInfo = req.userData;
    const idtrx = req.tokenID;
    var tipeModule = 1;

    const resp = await pool("mst_organizations as mo")
      .innerJoin("cdid_tenant_user as mtu", "mo.created_byid", "mtu.id")
      .where({
        "mo.idtenant": dcodeInfo.idtenant,
      })
      .select(
        "mo.id",
        "mo.orgname",
        "mo.orgdescription",
        "mo.idowner",
        "mo.idtenant",
        "mo.idsubtenant",
        "mo.orglevel",
        "mo.created_byid",
        " mtu.fullname as  created_by",
        "mo.orgcode"
      );
    var jsonbody = resp;
    console.log(resp);
    setTimeout(function () {
      res.status(200).json({ status: 200, data: jsonbody });
    }, 500);
  } catch (err) {
    next(err);
    console.log(err);
    res.status(500).json({ status: 500, data: "Error insert log Catch" });
  }
});
router.post("/addorganization", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var toknid = req.tokenID;
    const { orgname, orgdescription, apps } = req.body;
    console.log(">>>> APPLICATION INSERT " + JSON.stringify(dcodeInfo));

    const resp = await pool("mst_organizations")
      .returning(["id", "orgname"])
      .insert({
        orgname: orgname,
        orgdescription: orgdescription,
        idowner: dcodeInfo.idtenant,
        idtenant: dcodeInfo.idtenant,
        idsubtenant: 0,
        orglevel: dcodeInfo.leveltenant,
        created_byid: dcodeInfo.id,
      });
    if (resp.length > 0) {
      // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps.length));
      var insertObj = resp[0];
      if (apps.length > 0) {
        var idOrg = insertObj.id;
        let appInsert = [];
        await apps.map((app) => {
          let obj = {
            idorg: idOrg,
            idapp: app.id_application,
            idtenant: dcodeInfo.idtenant,
            idsubtenant: 0,
          };
          appInsert.push(obj);
        });
        const respApps = await pool("cdid_orgapplications")
          .returning(["id"])
          .insert(appInsert);
        console.log(respApps);
        if (respApps.length > 0) {
          await pool("trx_eventlog").insert({
            created_by: dcodeInfo.id,
            value: "Create Event",
            idapp: 0,
            idmenu: 0,
            description: "Create Group App in krakatoa",
            cdaction: 5,
            refevent: toknid,
          });

          res.status(200).json({ status: 200, data: resp[0] });
        } else {
          res.status(500).json({
            status: 500,
            data: "No Application on the organizations",
          });
        }
      } else {
        res.status(200).json({ status: 200, data: resp[0] });
      }
    } else {
    }

    // }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert organization" });
  }
});
router.get("/retriveByTenantAndOrgId/:id", async (req, res, next) => {
  try {
    let systemdate = new Date();
    const authHeader = req.headers.authorization;
    var dcodeInfo = req.userData;
    var tipeModule = 1;
    const orgid = req.params.id;

    const resp = await pool("mst_organizations as mo")
      .innerJoin("cdid_tenant_user as mtu", "mo.created_byid", "mtu.id")
      .where({
        "mo.idtenant": dcodeInfo.idtenant,
        "mo.id": orgid,
      })
      .select(
        "mo.id",
        "mo.orgname",
        "mo.orgdescription",
        "mo.idowner",
        "mo.idtenant",
        "mo.idsubtenant",
        "mo.orglevel",
        "mo.created_byid",
        " mtu.fullname as  created_by",
        "mo.orgcode"
      );
    var jsonbody = resp;

    res.status(200).json({ status: 200, data: jsonbody });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error get org data" });
  }
});

router.post("/editorganization", async (req, res, next) => {
  try {
    let systemdate = new Date();
    var dcodeInfo = req.userData;
    var toknid = req.tokenID;
    const { organization, apps } = req.body;
    console.log(
      organization.name,
      organization.description,
      Date.now(),
      organization.id
    );

    let now = new Date();
    now.setHours(now.getHours() + 7);
    let orgResult = await pool("mst_organizations")
      .returning(["id", "orgname", "orgdescription"])
      .where("id", organization.id)
      .update({
        orgname: organization.name,
        orgdescription: organization.description,
        updated_at: poo.fn.now(),
      });

    if (orgResult.length > 0) {
      await pool("cdid_orgapplications")
        .where({ idorg: organization.id })
        .del();

      var appsToInsert = [];
      console.log("cek panjang apps: " + apps.length);
      if (apps.length > 0) {
        await apps.map((app) => {
          let appToInsert = {
            idorg: organization.id,
            idapp: app.id_application,
            idtenant: dcodeInfo.idtenant,
            idsubtenant: 0,
          };
          appsToInsert.push(appToInsert);
        });

        let updateApp = await pool("cdid_orgapplications")
          .returning(["id"])
          .insert(appsToInsert);

        if (updateApp.length > 0) {
          await pool("trx_eventlog").insert({
            created_by: dcodeInfo.id,
            value: "Update Event",
            idapp: 0,
            idmenu: 0,
            description: "Update Group App in krakatoa",
            cdaction: 6,
            refevent: toknid,
          });
          res.status(200).json({ status: 200, data: updateApp[0] });
        } else {
          res.status(500).json({
            status: 500,
            data: "No Application on the organizations",
          });
        }
      }
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert organization" });
  }
});

router.post("/deleteorganization", async (req, res, next) => {
  try {
    let systemdate = new Date();
    var dcodeInfo = req.userData;
    var toknid = req.tokenID;
    const { organization } = req.body;
    console.log(organization);
    // console.log(">>>> APPLICATION INSERT "+JSON.stringify(apps));

    let resp = await pool("mst_organizations")
      .where({ id: organization.id })
      .del();
    resp = await pool("cdid_orgapplications")
      .where({
        idorg: organization.id,
      })
      .del();

    await pool("trx_eventlog").insert({
      created_by: dcodeInfo.id,
      value: "Delete Event",
      idapp: 0,
      idmenu: 0,
      description: "Delete Group App in krakatoa",
      cdaction: 7,
      refevent: toknid,
    });
    res
      .status(200)
      .json({ status: 200, data: "Organization data deleted successfully" });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error Delete organization" });
  }
});

module.exports = router;
