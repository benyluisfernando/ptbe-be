const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("../../routes/services/utils/encryptdecryptjwt");
const logger = require("../../routes/services/utils/logservices");
const moment = require("moment");
const checkAuth = require("../../middleware/check-auth");
const passEnc = require("./utils/password/encryptionPass");
const logs = require("./utils/logger");
//router.use(checkAuth);
/*POST END*/

router.post("/createcompany", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let lvlTenant = parseInt(dcodeInfo.leveltenant);
  var apps = dcodeInfo.apps[0];
  var jsonResult = [];
  var query = "";
  // res.status(200).json({ status: 200, data: apps });
  try {
    const {
      companyname,
      companycode,
      companyemail,
      phone,
      address,
      nik,
      npwp,
      tipenik,
    } = req.body;
    if (lvlTenant > 1)
      res.status(500).json({ status: 500, data: "Not authorized" });
    //*********** SUB TENANT CREATION ########### */
    //************* Check if Tenant exist */
    //query =
    //   "SELECT * FROM cdid_tenant WHERE soundex(tnname) = soundex($1);";
    // const cekTenantResp = await pool.query(query, [companyname]);

    const cekTenantResp = await pool
      .select("*")
      .from("cdid_tenant")
      .where("ttnname", companyname);

    if (cekTenantResp.length > 0) {
      res.status(200).json({ status: 202, data: "Company data already taken" });
    } else {
      // query =
      //   "INSERT INTO public.cdid_tenant(tnname, tnstatus, tntype, cdidowner, tnflag, tnparentid, cdtenant) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *";
      // const insertTenantResp = await pool.query(query, [
      // companyname,
      // 1,
      // 3,
      // 1,
      // 2,
      // dcodeInfo.idtenant,
      // companycode,
      // ]);

      const insertTenantResp = await pool("cdid_tenant")
        .returning(["*"])
        .insert({
          tnname: companyname,
          tnstatus: 1,
          tntype: 3,
          cdidowner: 1,
          tnflag: 2,
          tnparentid: dcodeInfo.idtenant,
          cdtenant: companycode,
        });

      if (insertTenantResp.length > 0) {
        let tenantResp = insertTenantResp[0];
        // query =
        //   "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *";
        // const insertBioCorel = await pool.query(query, [
        // companyname,
        // companyemail,
        // phone,
        // address,
        // 2,
        // nik,
        // npwp,
        // tenantResp.id,
        // tipenik,
        // ]);

        const insertBioCorel = await pool("mst_biodata_corell")
          .returning(["*"])
          .insert({
            bioname: companyname,
            bioemailactive: companyemail,
            biophoneactive: phone,
            bioaddress: address,
            bioidcorel: 2,
            bionik: nik,
            bionpwp: npwp,
            biocorelobjid: tenantResp.id,
            bioidtipenik: tipenik,
          });

        if (insertBioCorel.length > 0) {
          jsonResult = insertBioCorel;
          res.status(200).json({ status: 200, data: jsonResult });
        }
      }
    }
    //   res.status(200).json({ status: 200, data: resp.rows });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert Issuer" });
  }
});

router.put("/createcompany/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let lvlTenant = parseInt(dcodeInfo.leveltenant);
  var apps = dcodeInfo.apps[0];
  var jsonResult = [];
  // res.status(200).json({ status: 200, data: apps });
  var query = "";
  try {
    const {
      companyname,
      status,
      companycode,
      companyemail,
      phone,
      address,
      nik,
      npwp,
      tipenik,
    } = req.body;
    if (lvlTenant > 1)
      res.status(500).json({ status: 500, data: "Not authorized" });
    //*********** SUB TENANT CREATION ########### */
    //************* Check if Tenant exist */
    // query =
    //   "UPDATE public.cdid_tenant SET tnname=$1, tnstatus=$2, tntype=$3, cdidowner=$4, tnflag=$5, tnparentid=$6, cdtenant=$7 WHERE id=$8 RETURNING *";
    // const insertTenantResp = await pool.query(query, [
    //   companyname,
    //   status,
    //   3,
    //   1,
    //   2,
    //   dcodeInfo.idtenant,
    //   companycode,
    //   req.params.id,
    // ]);

    const insertTenantResp = await pool("cdid_tenant")
      .returning(["*"])
      .where("id", req.params.id)
      .update({
        tnname: companyname,
        tnstatus: status,
        tntype: 3,
        cdidowner: 1,
        tnflag: 2,
        tnparentid: dcodeInfo.idtenant,
        cdtenant: companycode,
      });

    if (insertTenantResp.length > 0) {
      let tenantResp = insertTenantResp[0];
      // query =
      //   "UPDATE public.mst_biodata_corell SET bioname=$1, bioemailactive=$2, biophoneactive=$3, bioaddress=$4, bioidcorel=$5, bionik=$6, bionpwp=$7, bioidtipenik=$8 WHERE biocorelobjid=$9 and bioidcorel=$10 RETURNING *";
      // const insertBioCorel = await pool.query(query, [
      //   companyname,
      //   companyemail,
      //   phone,
      //   address,
      //   2,
      //   nik,
      //   npwp,
      //   tipenik,
      //   req.params.id,
      //   2,
      // ]);

      const insertBioCorel = await pool("mst_biodata_corell")
        .returning(["*"])
        .where("biocorelobjid", req.params.id)
        .andWhere("bioidcorel", 2)
        .update({
          bioname: companyname,
          bioemailactive: companyemail,
          biophoneactive: phone,
          bioaddress: address,
          bioidcorel: 2,
          bionik: nik,
          bionpwp: npwp,
          bioidtipenik: tipenik,
        });

      if (insertBioCorel.length > 0) {
        jsonResult = insertBioCorel;
        res.status(200).json({ status: 200, data: jsonResult });
      }
    }
    //   res.status(200).json({ status: 200, data: resp.rows });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error Update Issuer" });
  }
});

router.delete("/createcompany/:id", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let lvlTenant = parseInt(dcodeInfo.leveltenant);
  var apps = dcodeInfo.apps[0];
  var jsonResult = {};
  var query = "";
  // res.status(200).json({ status: 200, data: apps });
  try {
    if (lvlTenant > 1)
      res.status(500).json({ status: 500, data: "Not authorized" });
    //*********** SUB TENANT CREATION ########### */
    //************* Check if Tenant exist */
    // query = "Delete FROM public.cdid_tenant WHERE id=$1";
    // const insertTenantResp = await pool.query(query, [req.params.id]);

    const insertTenantResp = await pool("cdid_tenant")
      .where("id", req.params.id)
      .del();

    // query =
    //   "Delete FROM public.mst_biodata_corell WHERE biocorelobjid=$1 and bioidcorel=$2";
    // const insertBioCorel = await pool.query(query, [req.params.id, 2]);

    const insertBioCorel = await pool("mst_biodata_corell")
      .where("biocorelobjid", req.params.id)
      .andWhere("bioidcorel", 2)
      .del();

    jsonResult = { message: "Success deleted 1 records" };
    res.status(200).json({ status: 200, data: jsonResult });

    //   res.status(200).json({ status: 200, data: resp.rows });
  } catch (err) {
    next(err);
    res.status(500).json({ status: 500, data: "Error insert Tenant" });
  }
});

router.post("/createusers", async (req, res, next) => {
  var dcodeInfo = req.userData;
  let lvlTenant = parseInt(dcodeInfo.leveltenant);
  var apps = dcodeInfo.apps[0];
  var jsonResult = [];
  let query = "";
  s;
  // res.status(200).json({ status: 200, data: apps });
  try {
    const { idcompany, userslist } = req.body;
    if (lvlTenant > 1)
      res.status(500).json({ status: 500, data: "Not authorized" });
    //*********** SUB TENANT CREATION ########### */
    //************* Check if Tenant exist */
    let rowsUserInserted = [];
    let usersemailid = [];
    await userslist.map((user) => {
      // console.log(JSON.stringify(user));
      usersemailid.push(user.useremail);
    });
    // let queryTenant = format(
    //   "SELECT * FROM cdid_tenant_user WHERE userid IN (%L);",
    //   usersemailid
    // );
    // const cekTenantResp = await pool.query(queryTenant);

    const cekTenantResp = await conn
      .select("*")
      .from("cdid_tenant_user")
      .where("userid", usersemailid[0]);

    if (cekTenantResp.length > 0) {
      res.status(200).json({ status: 202, data: "some of user already taken" });
    } else {
      var objsToInsert = [];
      await userslist.map(async (userInsert) => {
        // console.log(userInsert);
        // var encryptrd = pool.query("select crypt("+userInsert.secret+", gen_salt('bf', 4));");
        // console.log(">>>>>>>>> ISI PASSWORD " + JSON.stringify(encryptrd));
        let pass = await passEnc.encryptPass(userInsert.secret);
        let userToInsert = [];
        userToInsert.push(pass);
        userToInsert.push(userInsert.fullname);
        userToInsert.push(userInsert.useremail);
        userToInsert.push(1);
        userToInsert.push(dcodeInfo.id);
        userToInsert.push(idcompany);
        userToInsert.push(2);
        userToInsert.push(1);
      });
      // let insertUsersQuery = format(
      //   "INSERT INTO cdid_tenant_user (pwd, fullname, userid,creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES %s returning *",
      //   objsToInsert
      // );

      const insertUsersQuery = await pool("cdid_tenant_user")
        .returning(["*"])
        .insert({
          pwd: usersemailid[0],
          fullname: usersemailid[1],
          userid: usersemailid[2],
          creator_stat: usersemailid[3],
          creator_byid: usersemailid[4],
          idtenant: usersemailid[5],
          leveltenant: usersemailid[6],
          active: usersemailid[7],
        });

      console.log(">>>>>>>> User insert " + insertUsersQuery);
      //   res.status(200).json({ status: 200, data: insertUsersQuery});

      // let insertingUsers = await pool.query(insertUsersQuery);

      if (insertUsersQuery.length > 0) {
        rowsUserInserted = insertUsersQuery;
        var objCorelsToInsert = [];
        await rowsUserInserted.map((insertedUser) => {
          // "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *"
          userslist.map((corelInsert) => {
            // let corelsToInsert =[];
            if (insertedUser.userid == corelInsert.useremail) {
              corelToInsert = [];
              corelToInsert.push(corelInsert.fullname);
              corelToInsert.push(corelInsert.useremail);
              corelToInsert.push("080000000000");
              corelToInsert.push("");
              corelToInsert.push(3);
              corelToInsert.push(corelInsert.nik);
              corelToInsert.push("");
              corelToInsert.push(insertedUser.id);
              corelToInsert.push(corelInsert.tipenik);
              // objCorelsToInsert.push(corelToInsert);
            }
          });
        });
        // let insertBioUsersQuery = format(
        //   "INSERT INTO mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES %L returning *",
        //   objCorelsToInsert
        // );
        // let insertingCorelsUser = await pool.query(insertBioUsersQuery);
        const insertBioUsersQuery = await pool("mst_biodata_corell")
          .returning(["*"])
          .insert({
            bioname: corelToInsert[0],
            bioemailactive: corelToInsert[1],
            biophoneactive: corelToInsert[2],
            bioaddress: corelToInsert[3],
            bioidcorel: corelToInsert[4],
            bionik: corelToInsert[5],
            bionpwp: corelToInsert[6],
            biocorelobjid: corelToInsert[7],
            bioidtipenik: corelToInsert[8],
          });

        console.log("Query corel >>>>>>>>>>>> " + insertBioUsersQuery);

        if (insertBioUsersQuery.length > 0) {
          // apps rowsUserInserted
          let orgid = apps.orgid;
          var objOrgUsrsAsgnToInsert = [];
          await rowsUserInserted.map((insertedUser) => {
            objOrgUsrAsgnToInsert = [];
            objOrgUsrAsgnToInsert.push(insertedUser.id);
            objOrgUsrAsgnToInsert.push(orgid);
            objOrgUsrAsgnToInsert.push(0);
            // objOrgUsrsAsgnToInsert.push(objOrgUsrAsgnToInsert);
          });

          // let insertQueryOrgAssignUser = format(
          //   "INSERT INTO cdid_orguserassign(userid, orgid, grpid) VALUES %L returning *",
          //   objOrgUsrsAsgnToInsert
          // );
          // let insertingOrgAssignUser = await pool.query(
          //   insertQueryOrgAssignUser
          // );

          const insertQueryOrgAssignUser = await pool("cdid_orguserassign")
            .returning(["*"])
            .insert({
              userid: objOrgUsrAsgnToInsert[0],
              orgid: objOrgUsrAsgnToInsert[1],
              grpid: objOrgUsrAsgnToInsert[2],
            });

          if (insertQueryOrgAssignUser.length > 0) {
            //Ini untuk memasukan hasil dari User inserted kedalam cdid_tenant_user_groupacl
            var userGroupAcl = [];
            await rowsUserInserted.map(async (insertedUser, index) => {
              await dcodeInfo.apps.map((app) => {
                userGroupAclToInsert = [];
                userGroupAclToInsert.push(insertedUser.id);
                userGroupAclToInsert.push(userslist[index].groupid);
                userGroupAclToInsert.push(dcodeInfo.idtenant);
                userGroupAclToInsert.push(app.id);
                userGroupAclToInsert.push(dcodeInfo.id);
                userGroupAclToInsert.push(2);
                // userGroupAcl.push(userGroupAclToInsert);
              });
            });
            // let insertUserGroupAcl = format(
            //   "INSERT INTO cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *",
            //   userGroupAcl
            // );
            // let insertACL = await pool.query(insertUserGroupAcl);

            const insertUserGroupAcl = await pool("cdid_tenant_user_groupacl")
              .returning(["*"])
              .insert({
                iduser: userGroupAclToInsert[0],
                idgroupuseracl: userGroupAclToInsert[1],
                idtenant: userGroupAclToInsert[2],
                idapplication: userGroupAclToInsert[3],
                created_byid: userGroupAclToInsert[4],
                grouptype: userGroupAclToInsert[5],
              });

            res.status(200).json({ status: 200, data: "Insert Success" });
          } else {
            res.status(500).json({
              status: 500,
              data: "No Org assigned User",
            });
          }
        } else {
          res.status(500).json({
            status: 500,
            data: "No user bio data Inserted",
          });
        }
      } else {
        res.status(500).json({
          status: 500,
          data: "No user Inserted",
        });
      }

      // INSERT INTO public.cdid_tenant_user(fullname, userid, pwd, creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES ($1, $2, crypt($3, gen_salt('bf', 4)), $4, $5, $6, $7, $8) RETURNING *
    }
    res.status(200).json({ status: 200, data: userslist });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert Users" });
  }
});

router.post("/createuserslast", async (req, res, next) => {
  var dcodeInfo = req.userData;

  let lvlTenant = parseInt(dcodeInfo.leveltenant);
  var apps = dcodeInfo.apps[0];
  var jsonResult = [];
  // res.status(200).json({ status: 200, data: apps });
  try {
    const { idcompany, userslist } = req.body;
    if (lvlTenant > 1)
      res.status(500).json({ status: 500, data: "Not authorized" });
    //*********** SUB TENANT CREATION ########### */
    //************* Check if Tenant exist */
    let rowsUserInserted = [];
    let usersemailid = [];
    await userslist.map((user) => {
      // console.log(JSON.stringify(user));
      usersemailid.push(user.useremail);
    });

    // let query = format(
    //   "SELECT * FROM cdid_tenant_user WHERE userid IN (%L);",
    //   usersemailid
    // );
    // const cekTenantResp = await pool.query(query);

    const cekTenantResp = await pool
      .select("*")
      .from("cdid_tenant_user")
      .where("userid", usersemailid[0]);

    if (cekTenantResp.length > 0) {
      res.status(200).json({ status: 202, data: "some of user already taken" });
    } else {
      var objsToInsert = [];
      await userslist.map(async (userInsert) => {
        // console.log(userInsert);
        //var encryptrd = pool.query("select crypt("+userInsert.secret+", gen_salt('bf', 4));");
        //console.log(">>>>>>>>> ISI PASSWORD "+JSON.stringify(encryptrd));
        let pass = await passEnc.encryptPass(userInsert.secret);
        let userToInsert = [];
        userToInsert.push(pass);
        userToInsert.push(userInsert.fullname);
        userToInsert.push(userInsert.useremail);
        userToInsert.push(1);
        userToInsert.push(dcodeInfo.id);
        userToInsert.push(idcompany);
        userToInsert.push(3);
        userToInsert.push(1);
        // let row = format(
        //   "(crypt(%L, gen_salt('bf', 4)),%L)",
        //   userInsert.secret,
        //   userToInsert
        // );
        // objsToInsert.push(row);
      });
      // let insertUsersQuery = format(
      //   "INSERT INTO cdid_tenant_user (pwd, fullname, userid,creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES %s returning *",
      //   objsToInsert
      // );
      //   res.status(200).json({ status: 200, data: insertUsersQuery});
      // let insertingUsers = await pool.query(insertUsersQuery);
      // let insertingUsers = await pool.raw(insertUsersQuery);

      const insertUsersQuery = await pool("cdid_tenant_user")
        .returning(["*"])
        .insert({
          pwd: usersemailid[0],
          fullname: usersemailid[1],
          userid: usersemailid[2],
          creator_stat: usersemailid[3],
          creator_byid: usersemailid[4],
          idtenant: usersemailid[5],
          leveltenant: usersemailid[6],
          active: usersemailid[7],
        });

      console.log(">>>>>>>> User insert " + insertUsersQuery);

      if (insertUsersQuery.length > 0) {
        rowsUserInserted = insertingUsers;
        var objCorelsToInsert = [];
        await rowsUserInserted.map((insertedUser) => {
          // "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *"
          userslist.map((corelInsert) => {
            // let corelsToInsert =[];
            if (insertedUser.userid == corelInsert.useremail) {
              corelToInsert = [];
              corelToInsert.push(corelInsert.fullname);
              corelToInsert.push(corelInsert.useremail);
              corelToInsert.push("080000000000");
              corelToInsert.push("");
              corelToInsert.push(3);
              corelToInsert.push(corelInsert.nik);
              corelToInsert.push("");
              corelToInsert.push(insertedUser.id);
              corelToInsert.push(corelInsert.tipenik);
              // objCorelsToInsert.push(corelToInsert);
            }
          });
        });
        // let insertBioUsersQuery = format(
        //   "INSERT INTO mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES %L returning *",
        //   objCorelsToInsert
        // );
        // let insertingCorelsUser = await pool.query(insertBioUsersQuery);

        const insertBioUsersQuery = await pool("mst_biodata_corell")
          .returning(["*"])
          .insert({
            bioname: corelToInsert[0],
            bioemailactive: corelToInsert[1],
            biophoneactive: corelToInsert[2],
            bioaddress: corelToInsert[3],
            bioidcorel: corelToInsert[4],
            bionik: corelToInsert[5],
            bionpwp: corelToInsert[6],
            biocorelobjid: corelToInsert[7],
            bioidtipenik: corelToInsert[8],
          });

        console.log("Query corel >>>>>>>>>>>> " + insertBioUsersQuery);

        if (insertBioUsersQuery.length > 0) {
          // apps rowsUserInserted
          let orgid = apps.orgid;
          var objOrgUsrsAsgnToInsert = [];
          await rowsUserInserted.map((insertedUser) => {
            objOrgUsrAsgnToInsert = [];
            objOrgUsrAsgnToInsert.push(insertedUser.id);
            objOrgUsrAsgnToInsert.push(orgid);
            objOrgUsrAsgnToInsert.push(0);
            // objOrgUsrsAsgnToInsert.push(objOrgUsrAsgnToInsert);
          });
          // let insertQueryOrgAssignUser = format(
          //   "INSERT INTO cdid_orguserassign(userid, orgid, grpid) VALUES %L returning *",
          //   objOrgUsrsAsgnToInsert
          // );
          // let insertingOrgAssignUser = await pool.query(
          //   insertQueryOrgAssignUser
          // );

          const insertQueryOrgAssignUser = await pool("cdid_orguserassign")
            .returning(["*"])
            .insert({
              userid: objOrgUsrAsgnToInsert[0],
              orgid: objOrgUsrAsgnToInsert[1],
              grpid: objOrgUsrAsgnToInsert[2],
            });

          if (insertQueryOrgAssignUser.length > 0) {
            //Ini untuk memasukan hasil dari User inserted kedalam cdid_tenant_user_groupacl
            var userGroupAcl = [];
            await rowsUserInserted.map(async (insertedUser, index) => {
              await dcodeInfo.apps.map((app) => {
                userGroupAclToInsert = [];
                userGroupAclToInsert.push(insertedUser.id);
                userGroupAclToInsert.push(userslist[index].groupid);
                userGroupAclToInsert.push(dcodeInfo.idtenant);
                userGroupAclToInsert.push(app.id);
                userGroupAclToInsert.push(dcodeInfo.id);
                userGroupAclToInsert.push(2);
                // userGroupAcl.push(userGroupAclToInsert);
              });
            });
            // let insertUserGroupAcl = format(
            //   "INSERT INTO cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *",
            //   userGroupAcl
            // );
            // let insertACL = await pool.query(insertUserGroupAcl);

            const insertUserGroupAcl = await pool("cdid_tenant_user_groupacl")
              .returning(["*"])
              .insert({
                iduser: userGroupAclToInsert[0],
                idgroupuseracl: userGroupAclToInsert[1],
                idtenant: userGroupAclToInsert[2],
                idapplication: userGroupAclToInsert[3],
                created_byid: userGroupAclToInsert[4],
                grouptype: userGroupAclToInsert[5],
              });

            res.status(200).json({ status: 200, data: "Insert Success" });
          } else {
            res.status(500).json({
              status: 500,
              data: "No Org assigned User",
            });
          }
        } else {
          res.status(500).json({
            status: 500,
            data: "No user bio data Inserted",
          });
        }
      } else {
        res.status(500).json({
          status: 500,
          data: "No user Inserted",
        });
      }

      // INSERT INTO public.cdid_tenant_user(fullname, userid, pwd, creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES ($1, $2, crypt($3, gen_salt('bf', 4)), $4, $5, $6, $7, $8) RETURNING *
    }
    res.status(200).json({ status: 200, data: userslist });
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error insert Users" });
  }
});

//############################################################################################

router.post("/initialtoken", async (req, res, next) => {
  var dcodeInfo = null;

  try {
    const { tokenNaked } = req.body;
    // let selectQuery =
    //   "SELECT id, created_by, created_date, updated_date, userinfo, expire_date, status FROM public.trx_sessionlog WHERE id=$1;";
    // let selectRespons = await pool.query(selectQuery, [tokenNaked]);
    // let selectQuery = format(
    //   "SELECT id, created_by, created_date, updated_date, userinfo, expire_date, status FROM public.trx_sessionlog WHERE id = %s",
    //   [tokenNaked]
    // );
    // let selectRespons = await pool.raw(selectQuery);

    const selectQuery = await pool
      .select("*")
      .from("trx_sessionlog")
      .where("id", tokenNaked);

    if (selectQuery.length > 0) {
      let resultDB = selectQuery[0];
      var token = jwttools.encryptdata(tokenNaked);
      res.status(200).json({ status: 200, data: token });
    } else {
      res.status(200).json({ status: 202, data: {} });
    }
  } catch (err) {
    console.log(err);
    next(err);
    res.status(500).json({ status: 500, data: "Error Select Users" });
  }
});

async function asyncForEach(array, callback) {
  // for (let index = 0; index < array.length; index++) {
  //   await callback(array[index], index, array);
  // }
}

router.post("/insertlog", async (req, res, next) => {
    // type : 1 info,2error
    const {
        created_by,
        value,
        idapp,
        idmenu,
        description,
        cdaction,
        refevent,
        valuejson,
    } = req.body;

    await pool("trx_eventlog").insert({
        created_by,
        value,
        idapp,
        idmenu,
        description,
        cdaction,
        refevent,
        valuejson,
    });
    //fileLog.info(`Saving event to eventlog `);

    res.status(200).json({ status: 200, data: "log saved" });
});

router.post("/eventlog", async (req, res, next) => {
    // type : 1 info,2error
    const { userId, eventVal, idapp, cdaction, description } = req.body;

    var dcodeInfo = req.userData;
    var tokenID = req.tokenID;
    const app = dcodeInfo.apps[0].appname
        ? dcodeInfo.apps[0].appname
        : "Unkwon App";
    const fileLog = logs.logging(
        dcodeInfo.apps[0].appname ? dcodeInfo.apps[0].appname : "krakatoa-logs"
    );

    await pool("trx_eventlog").insert({
        created_by: userId ? userId : 0,
        value: eventVal ? eventVal : 0,
        idapp: idapp ? idapp : 0,
        idmenu: 0,
        description: description ? description : 0,
        cdaction: cdaction ? cdaction : 0,
        refevent: req.tokenID ? req.tokenID : 0,
        valuejson: req.body ? JSON.stringify(req.body) : null,
    });
    fileLog.info(`Saving event to eventlog `);

    res.status(200).json({ status: 200, data: "log saved" });
});

module.exports = router;

// router.post("/initialtoken", async (req, res, next) => {
//   var dcodeInfo = null;

//     try {
//       const { tokenNaked } = req.body;
//       let selectQuery = "SELECT id, created_by, created_date, updated_date, userinfo, expire_date, status FROM public.trx_sessionlog WHERE id=$1;";
//       let selectRespons = await pool.query(selectQuery, [tokenNaked]);
//       if(selectRespons.rows.length > 0) {
//           let resultDB = selectRespons.rows[0];
//           var token = jwttools.encryptdata(tokenNaked);
//           res.status(200).json({ status: 200, data: token });
//       } else {
//           res.status(200).json({ status: 202, data: {} });
//       }

//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error Select Users" });
//     }

// });

// router.post("/createcompany", async (req, res, next) => {
//
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     let lvlTenant = parseInt(dcodeInfo.leveltenant);
//     var apps = dcodeInfo.apps[0];
//     var jsonResult = [];
//     // res.status(200).json({ status: 200, data: apps });
//     try {
//       const {
//         companyname,
//         companycode,
//         companyemail,
//         phone,
//         address,
//         nik,
//         npwp,
//         tipenik,
//       } = req.body;
//       if (lvlTenant > 1)
//         res.status(500).json({ status: 500, data: "Not authorized" });
//       //*********** SUB TENANT CREATION ########### */
//       //************* Check if Tenant exist */
//       var query =
//         "SELECT * FROM cdid_tenant WHERE soundex(tnname) = soundex($1);";
//       const cekTenantResp = await pool.query(query, [companyname]);
//       if (cekTenantResp.rows.length > 0) {
//         res
//           .status(200)
//           .json({ status: 202, data: "Company data already taken" });
//       } else {
//         query =
//           "INSERT INTO public.cdid_tenant(tnname, tnstatus, tntype, cdidowner, tnflag, tnparentid, cdtenant) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *";
//         const insertTenantResp = await pool.query(query, [
//           companyname,
//           1,
//           3,
//           1,
//           2,
//           dcodeInfo.idtenant,
//           companycode,
//         ]);
//         if (insertTenantResp.rows.length > 0) {
//           let tenantResp = insertTenantResp.rows[0];
//           query =
//             "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *";
//           const insertBioCorel = await pool.query(query, [
//             companyname,
//             companyemail,
//             phone,
//             address,
//             2,
//             nik,
//             npwp,
//             tenantResp.id,
//             tipenik,
//           ]);
//           if (insertBioCorel.rows.length > 0) {
//             jsonResult = insertBioCorel.rows;
//             res.status(200).json({ status: 200, data: jsonResult });
//           }
//         }
//       }
//       //   res.status(200).json({ status: 200, data: resp.rows });
//     } catch (err) {
//       res.status(500).json({ status: 500, data: "Error insert Issuer" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });

// router.put("/createcompany/:id", async (req, res, next) => {
//
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     let lvlTenant = parseInt(dcodeInfo.leveltenant);
//     var apps = dcodeInfo.apps[0];
//     var jsonResult = [];
//     // res.status(200).json({ status: 200, data: apps });
//     try {
//       const {
//         companyname,
//         status,
//         companycode,
//         companyemail,
//         phone,
//         address,
//         nik,
//         npwp,
//         tipenik,
//       } = req.body;
//       if (lvlTenant > 1)
//         res.status(500).json({ status: 500, data: "Not authorized" });
//       //*********** SUB TENANT CREATION ########### */
//       //************* Check if Tenant exist */
//       var query =
//         "UPDATE public.cdid_tenant SET tnname=$1, tnstatus=$2, tntype=$3, cdidowner=$4, tnflag=$5, tnparentid=$6, cdtenant=$7 WHERE id=$8 RETURNING *";
//       const insertTenantResp = await pool.query(query, [
//         companyname,
//         status,
//         3,
//         1,
//         2,
//         dcodeInfo.idtenant,
//         companycode,
//         req.params.id,
//       ]);
//       if (insertTenantResp.rows.length > 0) {
//         let tenantResp = insertTenantResp.rows[0];
//         query =
//           "UPDATE public.mst_biodata_corell SET bioname=$1, bioemailactive=$2, biophoneactive=$3, bioaddress=$4, bioidcorel=$5, bionik=$6, bionpwp=$7, bioidtipenik=$8 WHERE biocorelobjid=$9 and bioidcorel=$10 RETURNING *";
//         const insertBioCorel = await pool.query(query, [
//           companyname,
//           companyemail,
//           phone,
//           address,
//           2,
//           nik,
//           npwp,
//           tipenik,
//           req.params.id,
//           2,
//         ]);
//         if (insertBioCorel.rows.length > 0) {
//           jsonResult = insertBioCorel.rows;
//           res.status(200).json({ status: 200, data: jsonResult });
//         }
//       }
//       //   res.status(200).json({ status: 200, data: resp.rows });
//     } catch (err) {
//       res.status(500).json({ status: 500, data: "Error Update Issuer" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });
// router.delete("/createcompany/:id", async (req, res, next) => {
//
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     let lvlTenant = parseInt(dcodeInfo.leveltenant);
//     var apps = dcodeInfo.apps[0];
//     var jsonResult = {};
//     // res.status(200).json({ status: 200, data: apps });
//     try {
//       if (lvlTenant > 1)
//         res.status(500).json({ status: 500, data: "Not authorized" });
//       //*********** SUB TENANT CREATION ########### */
//       //************* Check if Tenant exist */
//       var query = "Delete FROM public.cdid_tenant WHERE id=$1";
//       const insertTenantResp = await pool.query(query, [req.params.id]);
//       query =
//         "Delete FROM public.mst_biodata_corell WHERE biocorelobjid=$1 and bioidcorel=$2";
//       const insertBioCorel = await pool.query(query, [req.params.id, 2]);
//       jsonResult = { message: "Success deleted 1 records" };
//       res.status(200).json({ status: 200, data: jsonResult });

//       //   res.status(200).json({ status: 200, data: resp.rows });
//     } catch (err) {
//       res.status(500).json({ status: 500, data: "Error insert Tenant" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });

// router.post("/createusers", async (req, res, next) => {
//
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     let lvlTenant = parseInt(dcodeInfo.leveltenant);
//     var apps = dcodeInfo.apps[0];
//     var jsonResult = [];
//     // res.status(200).json({ status: 200, data: apps });
//     try {
//       const { idcompany, userslist } = req.body;
//       if (lvlTenant > 1)
//         res.status(500).json({ status: 500, data: "Not authorized" });
//       //*********** SUB TENANT CREATION ########### */
//       //************* Check if Tenant exist */
//       let rowsUserInserted = [];
//       let usersemailid = [];
//       await userslist.map((user) => {
//         // console.log(JSON.stringify(user));
//         usersemailid.push(user.useremail);
//       });
//       let query = format(
//         "SELECT * FROM public.cdid_tenant_user WHERE userid IN (%L);",
//         usersemailid
//       );
//       const cekTenantResp = await pool.query(query);
//       if (cekTenantResp.rows.length > 0) {
//         res
//           .status(200)
//           .json({ status: 202, data: "some of user already taken" });
//       } else {
//         var objsToInsert = [];
//         await userslist.map((userInsert) => {
//           // console.log(userInsert);
//           //var encryptrd = pool.query("select crypt("+userInsert.secret+", gen_salt('bf', 4));");
//           //console.log(">>>>>>>>> ISI PASSWORD "+JSON.stringify(encryptrd));
//           let userToInsert = [];
//           userToInsert.push(userInsert.fullname);
//           userToInsert.push(userInsert.useremail);
//           userToInsert.push(1);
//           userToInsert.push(dcodeInfo.id);
//           userToInsert.push(idcompany);
//           userToInsert.push(2);
//           userToInsert.push(1);
//           let row = format(
//             "(crypt(%L, gen_salt('bf', 4)),%L)",
//             userInsert.secret,
//             userToInsert
//           );

//           objsToInsert.push(row);
//         });
//         let insertUsersQuery = format(
//           "INSERT INTO public.cdid_tenant_user (pwd, fullname, userid,creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES %s returning *",
//           objsToInsert
//         );
//         console.log(">>>>>>>> User insert " + insertUsersQuery);
//         //   res.status(200).json({ status: 200, data: insertUsersQuery});
//         let insertingUsers = await pool.query(insertUsersQuery);
//         if (insertingUsers.rows.length > 0) {
//           rowsUserInserted = insertingUsers.rows;
//           var objCorelsToInsert = [];
//           await rowsUserInserted.map((insertedUser) => {
//             // "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *"
//             userslist.map((corelInsert) => {
//               // let corelsToInsert =[];
//               if (insertedUser.userid == corelInsert.useremail) {
//                 corelToInsert = [];
//                 corelToInsert.push(corelInsert.fullname);
//                 corelToInsert.push(corelInsert.useremail);
//                 corelToInsert.push("080000000000");
//                 corelToInsert.push("");
//                 corelToInsert.push(3);
//                 corelToInsert.push(corelInsert.nik);
//                 corelToInsert.push("");
//                 corelToInsert.push(insertedUser.id);
//                 corelToInsert.push(corelInsert.tipenik);
//                 objCorelsToInsert.push(corelToInsert);
//               }
//             });
//           });
//           let insertBioUsersQuery = format(
//             "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES %L returning *",
//             objCorelsToInsert
//           );
//           console.log("Query corel >>>>>>>>>>>> " + insertBioUsersQuery);
//           let insertingCorelsUser = await pool.query(insertBioUsersQuery);
//           if (insertingCorelsUser.rows.length > 0) {
//             // apps rowsUserInserted
//             let orgid = apps.orgid;
//             var objOrgUsrsAsgnToInsert = [];
//             await rowsUserInserted.map((insertedUser) => {
//               objOrgUsrAsgnToInsert = [];
//               objOrgUsrAsgnToInsert.push(insertedUser.id);
//               objOrgUsrAsgnToInsert.push(orgid);
//               objOrgUsrAsgnToInsert.push(0);
//               objOrgUsrsAsgnToInsert.push(objOrgUsrAsgnToInsert);
//             });
//             let insertQueryOrgAssignUser = format(
//               "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid) VALUES %L returning *",
//               objOrgUsrsAsgnToInsert
//             );
//             let insertingOrgAssignUser = await pool.query(
//               insertQueryOrgAssignUser
//             );
//             if (insertingOrgAssignUser.rows.length > 0) {
//               //Ini untuk memasukan hasil dari User inserted kedalam cdid_tenant_user_groupacl
//               var userGroupAcl = [];
//               await rowsUserInserted.map(async (insertedUser, index) => {
//                 await dcodeInfo.apps.map((app) => {
//                   userGroupAclToInsert = [];
//                   userGroupAclToInsert.push(insertedUser.id);
//                   userGroupAclToInsert.push(userslist[index].groupid);
//                   userGroupAclToInsert.push(dcodeInfo.idtenant);
//                   userGroupAclToInsert.push(app.id);
//                   userGroupAclToInsert.push(dcodeInfo.id);
//                   userGroupAclToInsert.push(2);
//                   userGroupAcl.push(userGroupAclToInsert);
//                 });
//               });
//               let insertUserGroupAcl = format(
//                 "INSERT INTO public.cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *",
//                 userGroupAcl
//               );
//               let insertACL = await pool.query(insertUserGroupAcl);
//               res.status(200).json({ status: 200, data: "Insert Success" });
//             } else {
//               res.status(500).json({
//                 status: 500,
//                 data: "No Org assigned User",
//               });
//             }
//           } else {
//             res.status(500).json({
//               status: 500,
//               data: "No user bio data Inserted",
//             });
//           }
//         } else {
//           res.status(500).json({
//             status: 500,
//             data: "No user Inserted",
//           });
//         }

//         // INSERT INTO public.cdid_tenant_user(fullname, userid, pwd, creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES ($1, $2, crypt($3, gen_salt('bf', 4)), $4, $5, $6, $7, $8) RETURNING *
//       }
//       res.status(200).json({ status: 200, data: userslist });
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error insert Users" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });

// router.post("/createuserslast", async (req, res, next) => {
//
//   var dcodeInfo = null;
//   if (authHeader) {
//     TokenArray = authHeader.split(" ");
//     dcodeInfo = await jwttools.decryptdata(TokenArray[1]);
//     let lvlTenant = parseInt(dcodeInfo.leveltenant);
//     var apps = dcodeInfo.apps[0];
//     var jsonResult = [];
//     // res.status(200).json({ status: 200, data: apps });
//     try {
//       const { idcompany, userslist } = req.body;
//       if (lvlTenant > 1)
//         res.status(500).json({ status: 500, data: "Not authorized" });
//       //*********** SUB TENANT CREATION ########### */
//       //************* Check if Tenant exist */
//       let rowsUserInserted = [];
//       let usersemailid = [];
//       await userslist.map((user) => {
//         // console.log(JSON.stringify(user));
//         usersemailid.push(user.useremail);
//       });
//       let query = format(
//         "SELECT * FROM public.cdid_tenant_user WHERE userid IN (%L);",
//         usersemailid
//       );
//       const cekTenantResp = await pool.query(query);
//       if (cekTenantResp.rows.length > 0) {
//         res
//           .status(200)
//           .json({ status: 202, data: "some of user already taken" });
//       } else {
//         var objsToInsert = [];
//         await userslist.map((userInsert) => {
//           // console.log(userInsert);
//           //var encryptrd = pool.query("select crypt("+userInsert.secret+", gen_salt('bf', 4));");
//           //console.log(">>>>>>>>> ISI PASSWORD "+JSON.stringify(encryptrd));
//           let userToInsert = [];
//           userToInsert.push(userInsert.fullname);
//           userToInsert.push(userInsert.useremail);
//           userToInsert.push(1);
//           userToInsert.push(dcodeInfo.id);
//           userToInsert.push(idcompany);
//           userToInsert.push(3);
//           userToInsert.push(1);
//           let row = format(
//             "(crypt(%L, gen_salt('bf', 4)),%L)",
//             userInsert.secret,
//             userToInsert
//           );

//           objsToInsert.push(row);
//         });
//         let insertUsersQuery = format(
//           "INSERT INTO public.cdid_tenant_user (pwd, fullname, userid,creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES %s returning *",
//           objsToInsert
//         );
//         console.log(">>>>>>>> User insert " + insertUsersQuery);
//         //   res.status(200).json({ status: 200, data: insertUsersQuery});
//         let insertingUsers = await pool.query(insertUsersQuery);
//         if (insertingUsers.rows.length > 0) {
//           rowsUserInserted = insertingUsers.rows;
//           var objCorelsToInsert = [];
//           await rowsUserInserted.map((insertedUser) => {
//             // "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *"
//             userslist.map((corelInsert) => {
//               // let corelsToInsert =[];
//               if (insertedUser.userid == corelInsert.useremail) {
//                 corelToInsert = [];
//                 corelToInsert.push(corelInsert.fullname);
//                 corelToInsert.push(corelInsert.useremail);
//                 corelToInsert.push("080000000000");
//                 corelToInsert.push("");
//                 corelToInsert.push(3);
//                 corelToInsert.push(corelInsert.nik);
//                 corelToInsert.push("");
//                 corelToInsert.push(insertedUser.id);
//                 corelToInsert.push(corelInsert.tipenik);
//                 objCorelsToInsert.push(corelToInsert);
//               }
//             });
//           });
//           let insertBioUsersQuery = format(
//             "INSERT INTO public.mst_biodata_corell(bioname, bioemailactive, biophoneactive, bioaddress, bioidcorel, bionik, bionpwp, biocorelobjid, bioidtipenik) VALUES %L returning *",
//             objCorelsToInsert
//           );
//           console.log("Query corel >>>>>>>>>>>> " + insertBioUsersQuery);
//           let insertingCorelsUser = await pool.query(insertBioUsersQuery);
//           if (insertingCorelsUser.rows.length > 0) {
//             // apps rowsUserInserted
//             let orgid = apps.orgid;
//             var objOrgUsrsAsgnToInsert = [];
//             await rowsUserInserted.map((insertedUser) => {
//               objOrgUsrAsgnToInsert = [];
//               objOrgUsrAsgnToInsert.push(insertedUser.id);
//               objOrgUsrAsgnToInsert.push(orgid);
//               objOrgUsrAsgnToInsert.push(0);
//               objOrgUsrsAsgnToInsert.push(objOrgUsrAsgnToInsert);
//             });
//             let insertQueryOrgAssignUser = format(
//               "INSERT INTO public.cdid_orguserassign(userid, orgid, grpid) VALUES %L returning *",
//               objOrgUsrsAsgnToInsert
//             );
//             let insertingOrgAssignUser = await pool.query(
//               insertQueryOrgAssignUser
//             );
//             if (insertingOrgAssignUser.rows.length > 0) {
//               //Ini untuk memasukan hasil dari User inserted kedalam cdid_tenant_user_groupacl
//               var userGroupAcl = [];
//               await rowsUserInserted.map(async (insertedUser, index) => {
//                 await dcodeInfo.apps.map((app) => {
//                   userGroupAclToInsert = [];
//                   userGroupAclToInsert.push(insertedUser.id);
//                   userGroupAclToInsert.push(userslist[index].groupid);
//                   userGroupAclToInsert.push(dcodeInfo.idtenant);
//                   userGroupAclToInsert.push(app.id);
//                   userGroupAclToInsert.push(dcodeInfo.id);
//                   userGroupAclToInsert.push(2);
//                   userGroupAcl.push(userGroupAclToInsert);
//                 });
//               });
//               let insertUserGroupAcl = format(
//                 "INSERT INTO public.cdid_tenant_user_groupacl(iduser, idgroupuseracl, idtenant, idapplication, created_byid, grouptype) VALUES %L returning *",
//                 userGroupAcl
//               );
//               let insertACL = await pool.query(insertUserGroupAcl);
//               res.status(200).json({ status: 200, data: "Insert Success" });
//             } else {
//               res.status(500).json({
//                 status: 500,
//                 data: "No Org assigned User",
//               });
//             }
//           } else {
//             res.status(500).json({
//               status: 500,
//               data: "No user bio data Inserted",
//             });
//           }
//         } else {
//           res.status(500).json({
//             status: 500,
//             data: "No user Inserted",
//           });
//         }

//         // INSERT INTO public.cdid_tenant_user(fullname, userid, pwd, creator_stat, creator_byid, idtenant,  leveltenant, active) VALUES ($1, $2, crypt($3, gen_salt('bf', 4)), $4, $5, $6, $7, $8) RETURNING *
//       }
//       res.status(200).json({ status: 200, data: userslist });
//     } catch (err) {
//       console.log(err);
//       res.status(500).json({ status: 500, data: "Error insert Users" });
//     }
//   } else {
//     res.status(500).json({ status: 500, data: "Not authorized" });
//   }
// });
