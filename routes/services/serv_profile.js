const express = require("express"),
  app = express();
const router = express.Router();
const pool = require("../../connection/db");
const jwttools = require("./utils/encryptdecryptjwt");
const bcrypt = require("bcrypt");
const logger = require("./utils/logservices");
const moment = require("moment");
const validatePass = require("./utils/user_management/userValidation");
const sysParam = require("./utils/user_management/getSysParam");

const passwordUtil = require("./utils/password/passwordUtil");
const passwordEnc = require("./utils/password/encryptionPass");

let umParam;

const getParam = async () => {
  umParam = await sysParam.getUMParam();
};

/*POST END*/

const checkAuth = require("../../middleware/check-auth");
const eventLog = require("../../middleware/eventLog");
router.use(checkAuth);
router.use(eventLog);

router.get("/:id", async (req, res, next) => {
  const authHeader = req.headers.authorization;
  var dcodeInfo = null;
  TokenArray = authHeader.split(" ");
  dcodeInfo = req.userData;
  // var apps = dcodeInfo.apps[0];
  try {
    // const resp = await pool.query(
    //   "SELECT fullname, userid, tnname, leveltenant FROM cdid_tenant_user ctu JOIN cdid_tenant t ON ctu.idtenant = t.id WHERE ctu.id = $1",
    //   [req.params.id]
    // );

    const resp = await pool
      .select("fullname", "userid", "tnname", "leveltenant")
      .from("cdid_tenant_user as ctu")
      .innerJoin("cdid_tenant as t", "ctu.idtenant", "t.id")
      .where("ctu.id", req.params.id);

    res.status(200).json({ status: 200, data: resp[0] });
  } catch (err) {
    next(err);

    res.status(500).json({ status: 500, data: "No data Profile found" });
  }
});

router.put("/changepassword", async (req, res, next) => {
  getParam();

  //var apps = dcodeInfo.apps[0];

  try {
    const { id, oldpassword, newpassword } = req.body;
    console.log(id);
    // const check = await pool.query(
    //   "SELECT id FROM cdid_tenant_user WHERE id = $1 AND pwd=crypt($2, pwd) ",
    //   [id, oldpassword]
    // );

    const check = await pool
      .select("id", "pwd")
      .from("cdid_tenant_user")
      .where("id", id);

    let validPassword = await passwordEnc.comparePassword(
      oldpassword,
      check[0].pwd
    );

    if (validPassword === true) {
      // if (check.length > 0) {
      // const getOldPassword = await pool.query(
      //   "Select * from public.pass_hist where user_id = $1 order by id limit $2 ",
      //   [id, umParam.PASSWORD_HISTORY]
      // );

      const getOldPassword = await pool
        .select("*")
        .from("pass_hist")
        .where("user_id", id)
        .orderByRaw("id limit ?", [umParam.PASSWORD_HISTORY]);

      let oldPasswordList = [];
      let validNew = [];

      if (getOldPassword.length > 0) {
        await Promise.all(
          await getOldPassword.map(async (dt) => {
            //let validPass = await validatePass.validatePassword(newpassword, dt);
            console.log(newpassword);
            console.log(dt.pwd);
            let validPass = await passwordEnc.comparePassword(
              newpassword,
              dt.pwd
            );
            console.log(validPass);
            if (validPass === true) {
              validNew.push(false);
            }
          })
        );

        console.log(validNew);
        if (validNew.length > 0) {
          res.status(422).json({ status: 422, data: "Password not valid" });
          return;
        }
        let validateNewPass = await validatePass.validatePassword(newpassword);
        if (validateNewPass == false) {
          res.status(422).json({ status: 422, data: "Password not valid" });
          return;
        }
        await passwordUtil.insertPasswordHistory(id, newpassword);
        // const resp = await pool.query(
        //   "UPDATE cdid_tenant_user SET pwd = crypt($1,gen_salt('bf',4)) WHERE id = $2",
        //   [newpassword, id]
        // );

        let pass = await passwordEnc.encryptPass(newpassword);
        const resp = await pool("cdid_tenant_user")
          .returning(["id"])
          .where("id", id)
          .update({ pwd: pass });

        await passwordUtil.updateChangePasswordTime(id);
        res.status(200).json({ status: 200, data: resp });
      }
    } else {
      res.status(500).json({ status: 500, data: "Wrong Password" });
    }
  } catch (err) {
    console.log(err);
    next(err);

    res.status(500).json({ status: 500, data: "No data found" });
  }
});

module.exports = router;
