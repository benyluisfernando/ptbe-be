const pool = require("../../../connection/db");
const moment = require("moment");
insertLog = async (
  mactionfkid,
  description,
  locfkid,
  userfkid,
  refnum,
  routepath,
  status,
  tokencode
) => {
  try {
    // const resp = await pool.query(
    //   "INSERT INTO public.trx_logger(mactionfkid, description, locfkid, userfkid, refnum, routepath, status, tokencode) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING *",
    //   [
    //     mactionfkid,
    //     description,
    //     locfkid,
    //     userfkid,
    //     refnum,
    //     routepath,
    //     status,
    //     tokencode,
    //   ]
    // );

    const resp = await pool("trx_logger").insert({
      mactionfkid: mactionfkid,
      description: description,
      locfkid: locfkid,
      userfkid: userfkid,
      refnum: refnum,
      routepath: routepath,
      status: status,
      tokencode: tokencode,
    });
    if (resp.length < 1) {
      return false;
    } else {
      return true;
    }
  } catch (err) {
    return false;
  }
};

insertLogContract = async (
  mactionfkid,
  description,
  locfkid,
  userfkid,
  refnum,
  routepath,
  status,
  tokencode,
  contractcode
) => {
  try {
    // const resp = await pool.query(
    //   "INSERT INTO public.trx_logger(mactionfkid, description, locfkid, userfkid, refnum, routepath, status, tokencode, contractcode) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *",
    //   [
    //     mactionfkid,
    //     description,
    //     locfkid,
    //     userfkid,
    //     refnum,
    //     routepath,
    //     status,
    //     tokencode,
    //     contractcode,
    //   ]
    // );
    const resp = await pool("trx_logger").insert({
      mactionfkid: mactionfkid,
      description: description,
      locfkid: locfkid,
      userfkid: userfkid,
      refnum: refnum,
      routepath: routepath,
      status: status,
      tokencode: tokencode,
      contractcode: contractcode,
    });
    if (resp.length < 1) {
      return false;
    } else {
      return true;
    }
  } catch (err) {
    return false;
  }
};

module.exports = { insertLog, insertLogContract };
